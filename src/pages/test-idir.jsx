/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useState, useEffect } from "react";
import Responsive from "./responsive";
import Header from "../component/header/header";
import Logo from "../assets/Elements/Logo/LogoInnov.svg";
import Group_Scientist from "../assets/NotreDemarche/Quisommesnous/Pictures/Group_Scientist_1.svg";
import Woman_Scientist from "../assets/NotreDemarche/Quisommesnous/Pictures/Woman_Scientist_1.svg";
import Footer from "../component/footer/footer";
import Img1 from "../assets/NotreDemarche/Quisommesnous/Pictures/Group_Scientist_1.svg";
import Img2 from "../assets/NotreDemarche/Quisommesnous/Pictures/Woman_Scientist_1.svg";

const TestIdir = () => {
  const [windowWidth, setWindowWidth] = useState(window.innerWidth);
  useEffect(() => {
    // Fonction pour mettre à jour la largeur de la fenêtre
    const handleResize = () => {
      setWindowWidth(window.innerWidth);
    };

    // Ajoutez un écouteur d'événement pour surveiller les changements de taille de fenêtre
    window.addEventListener("resize", handleResize);

    // Retirez l'écouteur d'événement lors du démontage du composant
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  // Déterminez si la largeur de la fenêtre est inférieure à 1000px
  const isResponsive = windowWidth < 1000;

  return (
    <>
      {isResponsive && (
        <Responsive
          titreidir="IDIR® : LE TEST RÉVOLUTIONNAIRE DE DÉPISTAGE DE LA RÉSISTANCE À L'INSULINE DANS LES URINES IDIR® (IDENTIFICATION OF INSULIN RESISTANCE) "
          descIdir="Le test IDIR® permet de détecter et de quantifier la résistance à l’insuline, facteur de risque commun des maladies cardiométaboliques présent jusqu’à une décennie avant l’apparition des symptômes. Cette résistance à l’insuline est réversible via des thérapies non médicamenteuses, éducatives et médicales, permettant de retarder voire éviter le diabète de type 2, les maladies cardiovasculaires et les autres maladies cardiométaboliques. La résistance à l’insuline concerne actuellement entre 15,5 et 46,5% de la population mondiale. En dépistant la résistance à l’insuline de manière fiable, rapide et non invasive, le test IDIR® vient répondre à un besoin non comblé : connaître l’état de santé métabolique des populations très en amont de la survenue des maladies.
          Le test IDIR® est un test urinaire révolutionnaire développé par l’Unité Mixte de Recherche Sys2Diag  (lien externe en ouvrant une nouvelle fenetre vers https://www.sys2diag.cnrs.fr/fr/accueil/)
         (CNRS-ALCEN) et par SkillCell (lien externe en ouvrant une nouvelle fenetre vers https://www.skillcell-alcen.com/fr ), le test IDIR® utilise une technologie brevetée reposant sur le dosage enzymatique des acides aminés à chaîne ramifiée, les BCAAs. Le test IDIR® est conçu pour mesurer dans l’urine des taux anormaux de BCAAs, taux qui sont corrélés à la résistance à l’insuline. Selon de nombreuses études scientifiques, les BCAAs sont considérés comme les biomarqueurs métaboliques les plus précoces de la résistance à l’insuline, un facteur de risque clé des maladies cardiométaboliques. De plus, il est crucial de souligner qu'une fois dépistée à temps, la résistance à l'insuline peut être réversible pendant une fenêtre de 10 à 15 ans.
         "
         texteDesc="Le test IDIR® se distingue par sa simplicité et son caractère non invasif, offrant une solution abordable par rapport aux méthodes existantes (HOMA IR, …).  L'efficacité du test IDIR® a été prouvée par une étude de validation portant sur 330 patients en France, qui sera bientôt publiée dans The Lancet. Cette étude démontre son haut niveau de précision et de fiabilité. Le dispositif bénéficie d’un marquage CE et d’un enregistrement ANSM attestant de sa conformité aux normes européennes les plus strictes. Comparé aux méthodes actuelles d'évaluation de la résistance à l'insuline, le test IDIR offre une alternative prometteuse. En effet, ce test nécessite un seul prélèvement urinaire et fournit des résultats en seulement une heure. Le test IDIR® rend donc possible un dépistage massif de la résistance à l’insuline et constitue un outil innovant et concret dans la prévention des maladies cardiométaboliques. "
        />
      )}

      {!isResponsive && (
        <>
          <Header logo={Logo} />
          <section id="notre-demarche" style={sectionStyles}>
            <div style={contentStyles} className="container topbanniere">
              <div style={leftStyles}>
                <h1>QUI SOMMES-NOUS ?</h1>
              </div>
              <div style={rightStyles}>
                <p>Notre démarche / Qui sommes-nous ?</p>
              </div>
            </div>
          </section>
          <h2 className="container" id="titre2">
            Chapeau
          </h2>
          <section style={section2Styles} className="container">
            <div style={flexContainerStyles}>
              <div style={itemStyles}>
                <p style={textStyles}>
                  Innov Biotech est une start-up française déployant des
                  solutions de biotechnologies innovantes et accessibles au
                  service des entreprises et des populations en Europe et en
                  Afrique. Chez Innov Biotech, le bien-être de chacun est au
                  cœur de nos préoccupations, nous souhaitons offrir au plus
                  grand nombre et à grande échelle la possibilité d’agir sur sa
                  santé. <br />
                  <br />
                  Depuis sa création, Innov Biotech collabore avec des centres
                  de recherche et des biotechs françaises, afin de
                  commercialiser des tests de dépistages innovants pour la
                  détection des maladies non transmissibles. L’entreprise se
                  consacre à la production et la commercialisation du premier
                  test de dépistage de la résistance à l’insuline, le test IDIR,
                  et en est le distributeur exclusif sur tout le continent
                  africain.
                </p>
                <img
                  src={Group_Scientist}
                  alt="Group_Scientist"
                  style={imageStyles}
                />
              </div>
              <div style={itemStyles}>
                <img
                  src={Woman_Scientist}
                  alt="Woman_Scientist"
                  style={imageStyles}
                />
                <p style={textStyles}>
                  Le test IDIR est le premier test urinaire de dépistage de la
                  résistance à l’insuline permettant un dépistage précoce et
                  ciblé des personnes présentant un risque important de
                  développer des maladies cardiométaboliques.
                  <br />
                  <br />
                  Innov Biotech collabore également avec les agences de
                  réglementation pharmaceutique pour l’obtention des
                  autorisations de mise sur le marché (AMM) et assure la
                  disponibilité du test. De plus, l’entreprise soutient les
                  études de vérification et de validation du test IDIR.
                </p>
              </div>
            </div>
          </section>
          <Footer />
        </>
      )}
    </>
  );
};

const sectionStyles = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};

const contentStyles = {
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  width: "100%",
};

const leftStyles = {
  flex: 1,
};

const rightStyles = {
  display: "none",
  flex: 1,
  textAlign: "right",
};

const section2Styles = {
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  marginBottom: "10vh",
};

const flexContainerStyles = {
  display: "flex",
  justifyContent: "center",
  flexWrap: "wrap",
  width: "100%",
};

const itemStyles = {
  display: "flex",
  alignItems: "center",
  width: "100%",
  marginTop: "5vh",
};

const imageStyles = {
  flex: "0 0 auto", // Empêche l&#8217;image de s&#8217;étirer
  marginRight: "20px", // Espace entre l&#8217;image et le texte
};

const imageStyle = {
  flex: "0 0 auto", // Empêche l&#8217;image de s&#8217;étirer
  height: "200px"
};

const textStyles = {
  fontSize: "18px",
  padding: "3%",
  color: "#257c9e",
  lineHeight: "1.3",
  flex: "1", // Le texte prendra tout l&#8217;espace restant
};

export default TestIdir;
