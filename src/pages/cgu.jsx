/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useState, useEffect, useRef } from "react";
import { Link } from "react-router-dom";
import Header from "../component/header/header";
import Logo from "../assets/Elements/Logo/LogoInnov.svg";
import Footer from "../component/footer/footer";
import Footer1 from "../assets/Elements/Icons/Facebook.svg";
import Footer2 from "../assets/Elements/Icons/Instagram.svg";
import Footer3 from "../assets/Elements/Icons/Linkedin.svg";
import Footer4 from "../assets/Elements/Icons/Twitter.svg";
import Contact from "../component/contact/contact";
import LogoWhite from "../assets/Elements/Logo/LogoInnov_White.svg";

const CGU = () => {
  const [windowWidth, setWindowWidth] = useState(window.innerWidth);

  useEffect(() => {
    const handleResize = () => {
      setWindowWidth(window.innerWidth);
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const isResponsive = windowWidth < 1000;

  return (
    <>
      {isResponsive && (
        <>
          <div className="centered-logo" style={{ marginTop: "5vh" }}>
            <Link to="/">
              <img
                src={Logo}
                alt="Logo Innov Biotech"
                className="logoresponsive"
              />
            </Link>
          </div>
          <h2 className="container" id="titre2">
            INNO BIOTECH
          </h2>
          <p className="container dispositif-medical">
            <ul className="licgu">
              <li>
                <span>INNO BIOTECH</span> <br />
                E-mail{" "}
                <a href="mailto:contact@innovbiotech.co?subject=Demande%20de%20contact%20-%20innovbiotech">
                  contact@innovbiotech.co
                </a>
                <br />
                SIRET 89448414600010
              </li>
              <li>
                <span>Responsable de la publication</span>
                <br />
                Mr Simon GOMIS
              </li>

              <li>
                <span> Informatique et libertés</span>
                <br />
                Les informations recueillies font l&#8217;objet d&#8217;un
                traitement informatique destiné à répondre à vos questions ou à
                vous contacter et sont réservées exclusivement à l&#8217;usage
                de la société INNOV BIOTECH. Elles sont réservées exclusivement
                à l&#8217;usage de la société Innov Biotech et demeurent
                confidentielles.
                <br />
                <br />
                Conformément à la loi «Informatique et Libertés» du 6 janvier
                1978, les utilisateurs du site ayant saisi des données à
                caractère personnel disposent d&#8217;un droit d&#8217;accès, de
                rectification, de modification, de suppression ou
                d&#8217;opposition. Pour l&#8217;exercer, les utilisateurs
                peuvent s&#8217;adresser par e-mail à l&#8217;adresse suivante:
                INNOV BIOTECH{" "}
                <a href="mailto:contact@innovbiotech.co?subject=Demande%20de%20contact%20-%20innovbiotech">
                  contact@innovbiotech.co
                </a>{" "}
                <br />
                <br />
                Conformément au règlement général sur la protection des données
                du 27 avril 2016, la société INNOV BIOTECH. protège les données
                personnelles, en particulier celles des candidats à l’embauche
                dans son groupe. Ces données sont conservées pendant la durée
                strictement nécessaire à leur traitement par les services
                concernés et au maximum pendant une durée de cinq ans sauf
                opposition du candidat à cette conservation formulée par message
                électronique auprès de la société INNOV BIOTECH
                contact@innovbiotech.co. En cas de divulgation accidentelle
                d’une ou plusieurs données personnelles, le titulaire de ces
                données en sera avisé dans les meilleurs délais à compter de la
                découverte de cette divulgation.
              </li>

              <li>
                <span>Gestion des cookies</span>
                <br />
                En utilisant le site www.innovbiotech.co, vous consentez à
                l&#8217;utilisation des cookies. Vous pouvez gérer ces cookies
                et les désactiver à tout moment.
                <br />
                <br />
                Les cookies sont de simples fichiers texte qui enregistrent
                certaines informations de votre visite dans le but
                d&#8217;améliorer les services qui vous sont destinés et de
                faciliter la navigation ultérieure sur le site. Ils ont
                également vocation à permettre diverses mesures de
                fréquentation.
                <br />
                <br />
                L&#8217;utilisateur peut s&#8217;opposer à
                l&#8217;enregistrement de cookies ou les désactiver en
                configurant son navigateur, en consultant le menu d&#8217;aide
                de son navigateur.
              </li>

              <li>
                <span>Crédits photo et image</span>
                <br />
                INNOV BIOTECH contact@innovbiotech.co
              </li>
              <li>
                <span>Hôte</span>
                <br />
                ES-GROUP – IONOS - OVH
              </li>
            </ul>
          </p>

          <section className="blocconfiance">
            <h2>
              Nous sommes membres des pôles de compétitivité et d’innovation
            </h2>
            <div>
              <div className="blocconfiancediv">
                <a
                  href="https://frenchhealthcare-association.fr/"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <img
                    src="https://www.adechotech.fr/wp-content/uploads/2016/03/FrenchHealthcareLogo.png"
                    alt="Description de l&#8217;image 1"
                  />
                </a>
              </div>
              <div className="blocconfiancediv">
                <a
                  href="https://www.eurobiomed.org"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <img
                    src="https://www.eurobiomed.org/wp-content/uploads/2020/12/Logo-Eurobiomed-1-2.png"
                    alt="Description de l&#8217;image 2"
                  />
                </a>
              </div>
              <div className="blocconfiancediv">
                <a
                  href="https://www.medvallee.fr/fr"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <img
                    src="https://www.afssi-connexions.fr/wp-content/uploads/2022/12/Medvallee_logo.png"
                    alt="Description de l&#8217;image 3"
                  />
                </a>
              </div>
              <div className="blocconfiancediv">
                <a
                  href="https://www.africalink.fr/"
                  target="_blank"
                  rel="noopener noreferrer"
                >
                  <img
                    src="https://www.africalink.fr/logo.png"
                    alt="Description de l&#8217;image 4"
                  />
                </a>
              </div>
            </div>
          </section>
          <Contact />

          <footer id="footer" style={footerStyles}>
            <section className="container" style={containerStyles}>
              <div className="footer-line" style={lineStyles}>
                <div className="footer-logo">
                  <img src={LogoWhite} alt="Logo" />
                </div>
                <div className="footer-text" style={textStyles}>
                  <div className="footer-column">
                    <ul>
                      <li>
                        <a href="#" title="" target="_blank" rel="noreferrer">
                          <img src={Footer1} alt="Image RS" />
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div className="footer-column">
                    <ul>
                      <li>
                        <a href="#" title="" target="_blank" rel="noreferrer">
                          <img src={Footer2} alt="Image RS" />
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div className="footer-column">
                    <ul>
                      <li>
                        <a
                          href="https://www.linkedin.com/company/innovbiotech-france/?originalSubdomain=fr"
                          title=""
                          target="_blank"
                          rel="noreferrer"
                        >
                          <img src={Footer3} alt="Image RS" />
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div className="footer-column">
                    <ul>
                      <li style={{ marginTop: "5vh" }}>
                        <a href="#" title="" target="_blank" rel="noreferrer">
                          <img src={Footer4} alt="Image RS" />
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
              <p style={texteFooter}>
                Copyright © 2023 INNOV BIOTECH All rights reserved.
              </p>
            </section>
          </footer>
        </>
      )}

      {!isResponsive && (
        <>
          <Header logo={Logo} />
          <section id="notre-demarche">
            <div className="container topbanniere">
              <div>
                <h1>Conditions générales d&#8217;utilisation</h1>
              </div>
              <div>
                <p>Notre démarche / Notre démarche RSE</p>
              </div>
            </div>
          </section>

          <h2 className="container" id="titre2">
            INNO BIOTECH
          </h2>
          <p className="container dispositif-medical">
            <ul className="licgu">
              <li>
                <span>INNO BIOTECH</span> <br />
                E-mail{" "}
                <a href="mailto:contact@innovbiotech.co?subject=Demande%20de%20contact%20-%20innovbiotech">
                  contact@innovbiotech.co
                </a>
                <br />
                SIRET 89448414600010
              </li>
              <li>
                <span>Responsable de la publication</span>
                <br />
                Mr Simon GOMIS
              </li>

              <li>
                <span> Informatique et libertés</span>
                <br />
                Les informations recueillies font l&#8217;objet d&#8217;un
                traitement informatique destiné à répondre à vos questions ou à
                vous contacter et sont réservées exclusivement à l&#8217;usage
                de la société INNOV BIOTECH. Elles sont réservées exclusivement
                à l&#8217;usage de la société Innov Biotech et demeurent
                confidentielles.
                <br />
                <br />
                Conformément à la loi «Informatique et Libertés» du 6 janvier
                1978, les utilisateurs du site ayant saisi des données à
                caractère personnel disposent d&#8217;un droit d&#8217;accès, de
                rectification, de modification, de suppression ou
                d&#8217;opposition. Pour l&#8217;exercer, les utilisateurs
                peuvent s&#8217;adresser par e-mail à l&#8217;adresse suivante:
                INNOV BIOTECH{" "}
                <a href="mailto:contact@innovbiotech.co?subject=Demande%20de%20contact%20-%20innovbiotech">
                  contact@innovbiotech.co
                </a>{" "}
                <br />
                <br />
                Conformément au règlement général sur la protection des données
                du 27 avril 2016, la société INNOV BIOTECH. protège les données
                personnelles, en particulier celles des candidats à l’embauche
                dans son groupe. Ces données sont conservées pendant la durée
                strictement nécessaire à leur traitement par les services
                concernés et au maximum pendant une durée de cinq ans sauf
                opposition du candidat à cette conservation formulée par message
                électronique auprès de la société INNOV BIOTECH
                contact@innovbiotech.co. En cas de divulgation accidentelle
                d’une ou plusieurs données personnelles, le titulaire de ces
                données en sera avisé dans les meilleurs délais à compter de la
                découverte de cette divulgation.
              </li>

              <li>
                <span>Gestion des cookies</span>
                <br />
                En utilisant le site www.innovbiotech.co, vous consentez à
                l&#8217;utilisation des cookies. Vous pouvez gérer ces cookies
                et les désactiver à tout moment.
                <br />
                <br />
                Les cookies sont de simples fichiers texte qui enregistrent
                certaines informations de votre visite dans le but
                d&#8217;améliorer les services qui vous sont destinés et de
                faciliter la navigation ultérieure sur le site. Ils ont
                également vocation à permettre diverses mesures de
                fréquentation.
                <br />
                <br />
                L&#8217;utilisateur peut s&#8217;opposer à
                l&#8217;enregistrement de cookies ou les désactiver en
                configurant son navigateur, en consultant le menu d&#8217;aide
                de son navigateur.
              </li>

              <li>
                <span>Crédits photo et image</span>
                <br />
                INNOV BIOTECH contact@innovbiotech.co
              </li>
              <li>
                <span>Hôte</span>
                <br />
                ES-GROUP – IONOS - OVH
              </li>
            </ul>
          </p>
          <Footer />
        </>
      )}
    </>
  );
};
const footerStyles = {
  display: "flex",
  flexDirection: "column", // Disposition en colonnes
  alignItems: "center", // Centre les éléments horizontalement
  padding: "20px", // Ajoute un espace autour du footer
};

const texteFooter = {
  marginTop: "5vh",
  textAlign: "center",
  color: "white",
};

const containerStyles = {
  width: "100%", // Assurez-vous que le contenu du footer occupe toute la largeur
};

const lineStyles = {
  display: "flex",
  alignItems: "center", // Centre les éléments horizontalement
  justifyContent: "space-between", // Répartit les éléments à gauche et à droite
  width: "100%", // Assurez-vous que les lignes occupent toute la largeur disponible
};

const textStyles = {
  display: "flex",
  justifyContent: "space-between",
  width: "50%",
  alignItems: "center",
  margin: "auto",
  flexDirection: "row",
};
export default CGU;
