import Header from "../component/header/header";
import Logo from "../assets/Elements/Logo/LogoInnov_White.svg";
import Footer from "../component/footer/footer";
import Img1 from "../assets/Referencesetpartenaires/Logo/logo-cnrs-web.svg";
import Img2 from "../assets/Referencesetpartenaires/Logo/logo_ES_GROUP.svg";
import Img3 from "../assets/Referencesetpartenaires/Logo/Group3.svg";
import Img4 from "../assets/Referencesetpartenaires/Logo/Group17.svg";

const ReferencesPartenaires = () => {
  return (
    <>
       <div className="header-img">
    <Header  logo={Logo} / >
    </div>
      <section id="notre-demarche" style={sectionStyles}>
        <div style={contentStyles} className="container topbanniere">
          <div style={leftStyles}>
            <h1>ILS NOUS FONT CONFIANCE</h1>
          </div>
          <div style={rightStyles}>
            <p>Références et partenaires / Ils nous font confiance</p>
          </div>
        </div>
      </section>

      <h2 id="titre2" className="reftitre container">
        ILS NOUS FONT CONFIANCE
      </h2>
      <section className="container" style={flexContainerStyles}>
        <div style={flexItemStyles}>
          <div>
            <a
              href="URL_DU_PREMIER_LIEN"
              target="_blank"
              rel="noopener noreferrer"
            >
              <img src={Img1} alt="Description de l&#8217;image 1" />
            </a>
          </div>
          <div>
            <a
              href="URL_DU_DEUXIEME_LIEN"
              target="_blank"
              rel="noopener noreferrer"
            >
              <img src={Img2} alt="Description de l&#8217;image 2" />
            </a>
          </div>
          <div>
            <a
              href="URL_DU_TROISIEME_LIEN"
              target="_blank"
              rel="noopener noreferrer"
            >
              <img src={Img3} alt="Description de l&#8217;image 3" />
            </a>
          </div>
          <div>
            <a
              href="URL_DU_QUATRIEME_LIEN"
              target="_blank"
              rel="noopener noreferrer"
            >
              <img src={Img4} alt="Description de l&#8217;image 4" />
            </a>
          </div>
        </div>
      </section>
      <div id="topbot">
        <Footer />
      </div>
    </>
  );
};

const sectionStyles = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};

const contentStyles = {
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  width: "100%",
};

const leftStyles = {
  flex: 1,
};

const rightStyles = {
  display: "none",
  flex: 1,
  textAlign: "right",
};

const flexContainerStyles = {
  display: "flex",
  flexDirection: "column", // Pour empiler les éléments verticalement
  textAlign: "center",
};

const flexItemStyles = {
  display: "flex",
  justifyContent: "space-between", // Espacement égal entre les éléments
  alignItems: "center", // Pour centrer verticalement les éléments enfants
  gap: "20px", // Espacement entre les éléments
};

export default ReferencesPartenaires;
