// import Header from "../component/header/header";
// import Logo from "../assets/Elements/Logo/LogoInnov.svg";
// import Footer from "../component/footer/footer";

// const NousRejoindre = () => {
//   return (
//     <>
//       <Header logo={Logo} />
//       <section id="notre-demarche" style={sectionStyles}>
//         <div style={contentStyles} className="container topbanniere">
//           <div style={leftStyles}>
//             <h1>NOTRE POLITIQUE QUALITÉ</h1>
//           </div>
//           <div style={rightStyles}>
//             <p>Notre démarche / Notre politique qualité</p>
//           </div>
//         </div>
//       </section>

//       <div className="container" style={flexContainerStyles}>
//         <div className="cardpostuler" style={flexItemStyles}>
//           <h2 id="titre2">Retrouvez toutes nos offres d’emploi</h2>
//           <h3>Biologiste médical Chimie Analytique</h3>
//           <p>
//             Validation des analyses médicales et de leurs résultats : mise en
//             place des critères de validation technique et biologique, définition
//             des valeurs critiques, mise en place des contrôles et interprétation
//             des résultats…
//           </p>
//           <div style={flexContainerStyles}>
//             <div style={flexItemStyles}>
//               <div>CDI</div>
//               <div>Grenoble</div>
//               <div>Télétravail régulier</div>
//               <div>Il y a 3 jours</div>
//             </div>
//             <div style={flexItemStyles}>
//               <button>Postuler</button>
//             </div>
//           </div>
//         </div>
//       </div>

//       <Footer />
//     </>
//   );
// };

// const flexContainerStyles = {
//   display: "flex",
//   justifyContent: "space-between", // Ajustez cette valeur selon vos besoins
// };

// const flexItemStyles = {
//   flex: "0 0 auto", // Pour que les éléments ne s&#8217;étirent pas
//   textAlign: "left", // Alignement du texte à gauche
// };

// const sectionStyles = {
//   display: "flex",
//   justifyContent: "center",
//   alignItems: "center",
//   padding: "20px",
// };

// const contentStyles = {
//   display: "flex",
//   flexDirection: "row",
//   alignItems: "center",
//   width: "100%",
// };

// const leftStyles = {
//   flex: 1,
// };

// const rightStyles = {
//   flex: 1,
//   textAlign: "right",
// };

// export default NousRejoindre;
