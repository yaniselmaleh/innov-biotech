/* eslint-disable no-unused-vars */
import React from "react";
import Header from "../component/header/header";
import Logo from "../assets/Elements/Logo/LogoInnov_White.svg";
import Footer from "../component/footer/footer";

const ContactVision = () => {
  const handleDownloadCard = () => {
    // Créez un contenu VCF factice
    const vCardContent = `BEGIN:VCARD
VERSION:1.0
FN:Simon-Que GOMInnov
N:GOMIS;Simon-Que
ORG:Innov Biotech
TEL:+33 (0) 6 52 79 43 21
EMAIL:simon.gomis@innovbiotech.co
NOTE:Disponible par telephoone.
URL:http://www.innovbiotech.co
CATEGORIES:Work
PHOTO;VALUE=URL:https://www.centre-ressource-marseille.org/wp-content/uploads/2014/11/Simon-Q%C3%BB%C3%A9-GOMIS.jpeg
END:VCARD`;

    // Créez un objet Blob contenant le contenu VCF
    const blob = new Blob([vCardContent], { type: "text/vcard" });

    // Générez un URL pour le Blob
    const url = window.URL.createObjectURL(blob);

    // Créez un élément d&#8217;ancre pour le téléchargement
    const a = document.createElement("a");
    a.style.display = "none";
    a.href = url;
    a.download = "CarteDeVisite.vcf";

    // Ajoutez l&#8217;élément d&#8217;ancre au DOM et cliquez dessus pour déclencher le téléchargement
    document.body.appendChild(a);
    a.click();

    // Supprimez l&#8217;élément d&#8217;ancre après le téléchargement
    window.URL.revokeObjectURL(url);
    document.body.removeChild(a);
  };

  return (
    <>
    <div className="header-img">
    <Header  logo={Logo} / >
    </div>
     
      <section id="notre-demarche" style={sectionStyles}>
        <div style={contentStyles} className="container topbanniere">
          <div style={leftStyles}>
            <h1>CONTACT</h1>
          </div>
          <div style={rightStyles}>
            <p>Prise de contact</p>
          </div>
        </div>
      </section>

      <h2 id="titre2" className="reftitre container">
        Prise de contact
      </h2>
      <section className="container" style={flexContainerStyles}>
        <div style={flexItemStyles}>
          <div>
            <img
              src="https://www.centre-ressource-marseille.org/wp-content/uploads/2014/11/Simon-Q%C3%BB%C3%A9-GOMIS.jpeg"
              alt="Description de l&#8217;image 1"
              onClick={handleDownloadCard}
              style={{ cursor: "pointer" }}
            />
          </div>
        </div>
      </section>
      <div id="topbot">
        <Footer />
      </div>
    </>
  );
};

// Styles (conservez vos styles existants)
const sectionStyles = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};

const contentStyles = {
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  width: "100%",
};

const leftStyles = {
  flex: 1,
};

const rightStyles = {
  display: "none",
  flex: 1,
  textAlign: "right",
};

const flexContainerStyles = {
  display: "flex",
  flexDirection: "column",
  textAlign: "center",
};

const flexItemStyles = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  gap: "20px",
};
export default ContactVision;
