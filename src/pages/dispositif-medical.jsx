/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useState, useEffect } from "react";
import Responsive from "./responsive";
import Header from "../component/header/header";
import Logo from "../assets/Elements/Logo/LogoInnov_White.svg";
import Dispositif from "../assets/DispositifMedical/Pictures/Schema_Notre_Vision_v2.png";
import InsulineImg from "../assets/DispositifMedical/Pictures/IDIR-SKILLCELL_New.svg";
import Arrow from "../assets/DispositifMedical/Icon/Check.svg";
import Footer from "../component/footer/footer";

const DispositifMedical = () => {
  const [windowWidth, setWindowWidth] = useState(window.innerWidth);
  useEffect(() => {
    // Fonction pour mettre à jour la largeur de la fenêtre
    const handleResize = () => {
      setWindowWidth(window.innerWidth);
    };

    // Ajoutez un écouteur d'événement pour surveiller les changements de taille de fenêtre
    window.addEventListener("resize", handleResize);

    // Retirez l'écouteur d'événement lors du démontage du composant
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  const isResponsive = windowWidth < 1000;

  return (
    <>
      {isResponsive && (
        <Responsive
          titre1="Le test IDIR"
          textlien1="Découvrez le test IDIR"
          lien1="#contact"
          Img1={Dispositif}
          titre2="LE TEST RÉVOLUTIONNAIRE DE DÉPISTAGE DE LA RÉSISTANCE À L’INSULINE DANS LES URINES IDIR (IDentification of Insulin Resistance)"
          texte2="Le test IDIR  permet de détecter et de quantifier la résistance à l’insuline, facteur de risque commun des maladies cardiométaboliques présent jusqu’à une décennie avant l’apparition des symptômes. Cette résistance à l’insuline est réversible via des thérapies non médicamenteuses, éducatives et médicales, permettant de retarder voire éviter le diabète de type 2, les maladies cardiovasculaires et les autres maladies cardiométaboliques. La résistance à l’insuline concerne actuellement entre 15,5 et 46,5% de la population mondiale."
          Img2={InsulineImg}
          titre3="Le test IDIR  se distingue par sa simplicité et son caractère non invasif"
          texte3="Le test IDIR  se distingue par sa simplicité et son caractère non invasif, offrant une solution abordable par rapport aux méthodes existantes (HOMA IR, …). L’efficacité du test IDIR a été prouvée par une étude de validation portant sur 330 patients en France, qui sera bientôt publiée dans The Lancet. Cette étude démontre son haut niveau de précision et de fiabilité. Le dispositif bénéficie d’un marquage CE et d’un enregistrement ANSM attestant de sa conformité aux normes européennes les plus strictes.

          Comparé aux méthodes actuelles d’évaluation de la résistance à l’insuline, le test IDIR offre une alternative prometteuse. En effet, ce test nécessite un seul prélèvement urinaire et fournit des résultats en seulement une heure. Le test IDIR  rend donc possible un dépistage massif de la résistance à l’insuline et constitue un outil innovant et concret dans la prévention des maladies cardiométaboliques."
        />
      )}

      {!isResponsive && (
        <>
            <div className="header-img">
    <Header  logo={Logo} / >
    </div>
          <section id="notre-demarche" style={sectionStyles}>
            <div style={contentStyles} className="container topbanniere">
              <div style={leftStyles}>
                <h1>LE TEST IDIR</h1>
              </div>
              <div style={rightStyles}>
                <p>Notre Dispositif médical / Le test IDIR</p>
              </div>
            </div>
          </section>

          <h2 className="container" id="titre2">
            LE TEST RÉVOLUTIONNAIRE DE DÉPISTAGE DE LA RÉSISTANCE
            À L&#8217;INSULINE DANS LES URINES IDIR (IDentification of Insulin
            Resistance)
          </h2>
          <p className="container dispositif-medical">
            Le test IDIR  permet de détecter et de quantifier la résistance à
            l’insuline, facteur de risque commun des maladies cardiométaboliques
            présent jusqu’à une décennie avant l’apparition des symptômes. Cette
            résistance à l’insuline est réversible via des thérapies non
            médicamenteuses, éducatives et médicales, permettant de retarder
            voire éviter le diabète de type 2, les maladies cardiovasculaires et
            les autres maladies cardiométaboliques. La résistance à l’insuline
            concerne actuellement entre <b>15,5</b> et <b>46,5%</b> de la
            population mondiale.
            <br />
            <br />
            En dépistant la résistance à l’insuline de manière fiable, rapide et
            non invasive, le test IDIR  vient répondre à un besoin non comblé :
            connaître l’état de santé métabolique des populations très en amont
            de la survenue des maladies. Le test IDIR  est un test urinaire
            révolutionnaire développé par{" "}
            <a
              href="https://www.sys2diag.cnrs.fr/fr/accueil/"
              target="_blank"
              rel="noreferrer"
            >
              l’Unité Mixte de Recherche Sys2Diag
            </a>{" "}
            (CNRS-ALCEN) et par{" "}
            <a
              href="https://www.skillcell-alcen.com/fr"
              target="_blank"
              rel="noreferrer"
            >
              SkillCell
            </a>
            , le test IDIR  utilise une technologie brevetée reposant sur le
            dosage enzymatique des acides aminés à chaîne ramifiée, les BCAAs.
            Le test IDIR  est conçu pour mesurer dans l’urine des taux anormaux
            de BCAAs, taux qui sont corrélés à la résistance à l’insuline. Selon
            de nombreuses études scientifiques, les BCAAs sont considérés comme
            les biomarqueurs métaboliques les plus précoces de la résistance à
            l’insuline, un facteur de risque clé des maladies
            cardiométaboliques. De plus, il est crucial de souligner
            qu&#8217;une fois dépistée à temps, la résistance à l&#8217;insuline
            peut être réversible pendant une fenêtre de 10 à 15 ans.
          </p>

          <div className="bgimg">
            <img src={Dispositif} alt="Dispositif" className="img" />
          </div>

          <p className="container dispositif-medical">
            Le test IDIR  se distingue par sa simplicité et son caractère non
            invasif, offrant une solution abordable par rapport aux méthodes
            existantes (HOMA IR, …). L&#8217;efficacité du test IDIR a été
            prouvée par une étude de validation portant sur 330 patients en
            France, qui sera bientôt publiée dans The Lancet. Cette étude
            démontre son haut niveau de précision et de fiabilité. Le dispositif
            bénéficie d’un marquage CE et d’un enregistrement ANSM attestant de
            sa conformité aux normes européennes les plus strictes.
            <br />
            <br />
            Comparé aux méthodes actuelles d&#8217;évaluation de la résistance à
            l&#8217;insuline, le test IDIR offre une alternative prometteuse. En
            effet, ce test nécessite un seul prélèvement urinaire et fournit des
            résultats en seulement une heure. Le test IDIR  rend donc possible
            un dépistage massif de la résistance à l’insuline et constitue un
            outil innovant et concret dans la prévention des maladies
            cardiométaboliques.
          </p>

          <div className="bgblue">
            <section style={section2Styles} className="container">
              <div style={flexContainerStyles}>
                <div style={itemStyles}>
                  <img
                    src={InsulineImg}
                    alt="InsulineImg"
                    style={imageStyles}
                  />
                  <ul>
                    <div style={listItemStyles}>
                      <img
                        src={Arrow}
                        alt="InsulineImg"
                        style={listItemImageStyles}
                      />
                      <li>
                        Nature Non-Invasive : Le test, réalisé sur prélèvement
                        urinaire, garantit un déploiement simple et une
                        expérience sans inconfort pour le patient.
                      </li>
                    </div>
                    <div style={listItemStyles}>
                      <img
                        src={Arrow}
                        alt="InsulineImg"
                        style={listItemImageStyles}
                      />
                      <li>
                        Quantification précise : Par son caractère quantitatif,
                        IDIR permet de suivre l’évolution de la résistance à
                        l’insuline et d’objectiver en temps réel l’impact des
                        changements.
                      </li>
                    </div>
                    <div style={listItemStyles}>
                      <img
                        src={Arrow}
                        alt="InsulineImg"
                        style={listItemImageStyles}
                      />
                      <li>
                        Rapidité : Jusqu’à 45 prélèvements urinaires peuvent
                        être analysés simultanément avec un résultat en 60
                        minutes, alliant ainsi efficacité et célérité.
                      </li>
                    </div>
                    <div style={listItemStyles}>
                      <img
                        src={Arrow}
                        alt="InsulineImg"
                        style={listItemImageStyles}
                      />
                      <li>
                        Simplicité d’utilisation : Le test IDIR  nécessite un
                        équipement technique limité. Sa mise en œuvre est
                        faisable dans tout laboratoire d’analyse biologique ou
                        en point of care.
                      </li>
                    </div>
                    <div style={listItemStyles}>
                      <img
                        src={Arrow}
                        alt="InsulineImg"
                        style={listItemImageStyles}
                      />
                      <li>
                        Coût abordable : IDIR représente une solution
                        économique attractive en comparaison aux tests sanguins
                        existants. Ainsi le test urinaire IDIR révolutionne
                        l’approche de dépistage de la résistance à l’insuline et
                        de la prévention des maladies cardiométaboliques,
                        offrant une solution précise, rapide et économique,
                        adaptée aussi bien pour une utilisation individuelle que
                        pour des campagnes de dépistage à grande échelle.
                      </li>
                    </div>
                  </ul>
                </div>
              </div>
            </section>
          </div>

          <h2 className="container" id="titre2">
            PRODUCTION LOCALE : UNE ÉTAPE VERS LA SOUVERAINETÉ PHARMACEUTIQUE
          </h2>
          <p className="container dispositif-medical">
            L’un des atouts majeurs du test IDIR est sa capacité à être
            produit localement. Son design permet un transfert de technologie
            aisé, ouvrant la voie à la création d’une unité de production
            nationale. Cette autonomie dans la production renforce non seulement
            la souveraineté pharmaceutique des pays mais offre également une
            solution économiquement avantageuse.
          </p>
          <h2 className="container" id="titre2">
            UNE INNOVATION MAJEURE POUR PRÉVENIR LES MALADIES CARDIOMÉTABOLIQUES
            DANS LE MONDE
          </h2>
          <p className="container dispositif-medical">
            Le test IDIR , distribué par Innov Biotech, est bien plus qu’un
            simple outil de dépistage. Il symbolise une approche proactive face
            à l’épidémie croissante des maladies cardiométaboliques. Avec sa
            facilité d’utilisation, sa fiabilité certifiée, son faible coût
            économique et la possibilité d’une production locale pour les autres
            pays que la France. Le test IDIR  s’inscrit comme une avancée
            majeure dans la prévention des maladies cardiométaboliques dans le
            monde. En rendant possible un dépistage massif de la résistance à
            l’insuline à l’échelle mondiale, IDIR constitue un outil innovant
            et concret dans la prévention des maladies cardiométaboliques.
          </p>
          <Footer />
        </>
      )}
    </>
  );
};

const sectionStyles = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};

const contentStyles = {
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  width: "100%",
};

const leftStyles = {
  flex: 1,
};

const rightStyles = {
  display: "none",
  flex: 1,
  textAlign: "right",
};

const section2Styles = {
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
};

const flexContainerStyles = {
  display: "flex",
  justifyContent: "center",
  flexWrap: "wrap",
  width: "100%",
};

const itemStyles = {
  display: "flex",
  alignItems: "center",
  width: "100%",
  marginTop: "10vh",
  marginBottom: "10vh",
};

const imageStyles = {
  flex: "0 0 auto", // Empêche l&#8217;image de s&#8217;étirer
  marginRight: "20px", // Espace entre l&#8217;image et le texte
};

const listItemStyles = {
  display: "flex",
  alignItems: "flex-start",
  justifyContent: "left", // Centrer horizontalement
  marginTop: "5vh",
};

const listItemImageStyles = {
  flex: "0 0 auto",
  marginRight: "20px",
  width: "30px",
  marginBottom: "3vh",
};

export default DispositifMedical;
