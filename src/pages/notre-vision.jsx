/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useState, useEffect } from "react";
import Responsive from "./responsive";
import Header from "../component/header/header";
import Logo from "../assets/Elements/Logo/LogoInnov_White.svg";
import Dispositif from "../assets/DispositifMedical/Pictures/Schema_Notre_Vision_v2.png";
import Vision from "../assets/NotreDemarche/NotreVision/Schéma_Notre_Vision.png";
import Footer from "../component/footer/footer";

const NotreVision = () => {
  const [windowWidth, setWindowWidth] = useState(window.innerWidth);
  useEffect(() => {
    // Fonction pour mettre à jour la largeur de la fenêtre
    const handleResize = () => {
      setWindowWidth(window.innerWidth);
    };

    // Ajoutez un écouteur d'événement pour surveiller les changements de taille de fenêtre
    window.addEventListener("resize", handleResize);

    // Retirez l'écouteur d'événement lors du démontage du composant
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  // Déterminez si la largeur de la fenêtre est inférieure à 1000px
  const isResponsive = windowWidth < 1000;
  return (
    <>
      {isResponsive && (
        <Responsive
          titre1="NOTRE VISION"
          textlien1="Découvrez le test IDIR"
          lien1="#contact"
          Img1={Vision}
          titre2="NOTRE OBJECTIF : ENRAYER L’ÉPIDÉMIE MONDIALE CAUSÉE PAR LES MALADIES CARDIOMÉTABOLIQUES"
          texte2="Selon le rapport 2022 de l’OMS (Organisation mondiale de la santé) intitulé 'Invisible numbers', les maladies non transmissibles, telles que les maladies cardiovasculaires et le diabète de type 2, représentent plus de 70% des décès dans le monde. Ces maladies ont des causes multiples, à la fois sociales, environnementales, commerciales et génétiques, et touchent toutes les régions du globe. Malheureusement, chaque année, 17 millions de personnes de moins de 70 ans meurent prématurément des suites de ces maladies."
          Img2={Dispositif}
          titre3="Cependant, bon nombre de ces décès prématurés pourraient être évités. Pour cela il est primordial de dépister les maladies
          cardiométaboliques (diabètes…) et de se concentrer sur la réduction
          des facteurs de risque associés à ces maladies, tels qu&#8217;une
          mauvaise alimentation et la sédentarité. Heureusement, des solutions
          peu coûteuses existent pour les gouvernements et les autres acteurs
          impliqués, afin de réduire ces facteurs de risque modifiables, tel
          que le programme PREDIA proposé par l’ONG AcSan."
          texte3="Parmi les facteurs de risque les plus importants, on retrouve la
          résistance à l&#8217;insuline, qui est largement favorisée par
          l’alcool, la sédentarité, le tabac, l’obésité, le VIH/TARV et
          l’hérédité. Elle est aujourd&#8217;hui reconnue comme un facteur
          majeur du syndrome métabolique et de nombreuses maladies, notamment
          le diabète de type 2, les maladies cardiovasculaires, certains
          cancers et d’autres maladies cardiométaboliques. En réalisant un
          dépistage précoce de la résistance à l&#8217;insuline et en mettant
          en place des interventions concernant l&#8217;alimentation et
          l&#8217;activité physique, il est possible de prévenir de manière
          économique les maladies non transmissibles associées au syndrome
          métabolique"
          text5="Parmi les facteurs de risque les plus importants, on retrouve la
          résistance à l&#8217;insuline, qui est largement favorisée par
          l’alcool, la sédentarité, le tabac, l’obésité, le VIH/TARV et
          l’hérédité. Elle est aujourd&#8217;hui reconnue comme un facteur
          majeur du syndrome métabolique et de nombreuses maladies, notamment
          le diabète de type 2, les maladies cardiovasculaires, certains
          cancers et d’autres maladies cardiométaboliques. En réalisant un
          dépistage précoce de la résistance à l&#8217;insuline et en mettant
          en place des interventions concernant l&#8217;alimentation et
          l&#8217;activité physique, il est possible de prévenir de manière
          économique les maladies non transmissibles associées au syndrome
          métabolique"
          text6=" En effet, il est important de noter que la résistance à
          l&#8217;insuline n&#8217;est pas une condamnation à vie. Si elle est
          détectée à temps, les personnes concernées possèdent une fenêtre de
          10 à 15 ans durant laquelle ils peuvent retrouver une sensibilité à
          l’insuline. Pour cela, des mesures appropriées doivent être mises en
          place, comprenant des changements de mode de vie tels qul&#8217;une
          alimentation équilibrée et une activité physique régulière."
          text7="Chez Innov Biotech, notre mission est de lutter contre les maladies
          cardiométaboliques en proposant le test IDIR. Ce test offre une
          solution fiable, efficace et reproductible pour le dépistage de la
          résistance à l&#8217;insuline. En combinant la détection des
          symptômes cliniques de la résistance à l&#8217;insuline avec le test
          IDIR, nous permettons un diagnostic précis de cette condition au
          sein de la population, ainsi qu&#8217;une prévention des maladies
          cardiométaboliques. <br />
          Nous sommes déterminés à faire progresser la santé cardiovasculaire
          et métabolique, et nous espérons pouvoir contribuer à réduire
          l&#8217;impact de ces maladies à l&#8217;échelle mondiale.
          Rejoignez-nous dans notre lutte contre les maladies
          cardiométaboliques et découvrez comment notre test IDIR peut jouer
          un rôle essentiel dans la prévention et le dépistage précoce de ces
          affections."
          text8="Chez Innov Biotech, notre mission est de lutter contre les maladies
          cardiométaboliques en proposant le test IDIR. Ce test offre une
          solution fiable, efficace et reproductible pour le dépistage de la
          résistance à l&#8217;insuline. En combinant la détection des
          symptômes cliniques de la résistance à l&#8217;insuline avec le test
          IDIR, nous permettons un diagnostic précis de cette condition au
          sein de la population, ainsi qu&#8217;une prévention des maladies
          cardiométaboliques. <br />
          Nous sommes déterminés à faire progresser la santé cardiovasculaire
          et métabolique, et nous espérons pouvoir contribuer à réduire
          l&#8217;impact de ces maladies à l&#8217;échelle mondiale.
          Rejoignez-nous dans notre lutte contre les maladies
          cardiométaboliques et découvrez comment notre test IDIR peut jouer
          un rôle essentiel dans la prévention et le dépistage précoce de ces
          affections."
        />
      )}

      {!isResponsive && (
        <>
         <div className="header-img">
    <Header  logo={Logo} / >
    </div>
          <section id="notre-demarche" style={sectionStyles}>
            <div style={contentStyles} className="container topbanniere">
              <div style={leftStyles}>
                <h1>NOTRE VISION</h1>
              </div>
              <div style={rightStyles}>
                <p>Notre démarche / Notre vision</p>
              </div>
            </div>
          </section>

          <h2 className="container" id="titre2">
            NOTRE OBJECTIF : ENRAYER L’ÉPIDÉMIE MONDIALE CAUSÉE PAR LES MALADIES
            CARDIOMÉTABOLIQUES
          </h2>
          <p className="container dispositif-medical">
            Selon le rapport 2022 de l&#8217;OMS (Organisation mondiale de la
            santé) intitulé &quot;Invisible numbers&quot;, les maladies non
            transmissibles, telles que les maladies cardiovasculaires et le
            diabète de type 2, représentent plus de 70% des décès dans le monde.
            Ces maladies ont des causes multiples, à la fois sociales,
            environnementales, commerciales et génétiques, et touchent toutes
            les régions du globe. Malheureusement, chaque année, 17 millions de
            personnes de moins de 70 ans meurent prématurément des suites de ces
            maladies.
            <br />
            <br />
            Cependant, bon nombre de ces décès prématurés pourraient être
            évités. Pour cela il est primordial de dépister les maladies
            cardiométaboliques (diabètes…) et de se concentrer sur la réduction
            des facteurs de risque associés à ces maladies, tels qu&#8217;une
            mauvaise alimentation et la sédentarité. Heureusement, des solutions
            peu coûteuses existent pour les gouvernements et les autres acteurs
            impliqués, afin de réduire ces facteurs de risque modifiables, tel
            que le programme PREDIA proposé par l’ONG AcSan.
            <br />
            <br />
            Parmi les facteurs de risque les plus importants, on retrouve la
            résistance à l&#8217;insuline, qui est largement favorisée par
            l’alcool, la sédentarité, le tabac, l’obésité, le VIH/TARV et
            l’hérédité. Elle est aujourd&#8217;hui reconnue comme un facteur
            majeur du syndrome métabolique et de nombreuses maladies, notamment
            le diabète de type 2, les maladies cardiovasculaires, certains
            cancers et d’autres maladies cardiométaboliques. En réalisant un
            dépistage précoce de la résistance à l&#8217;insuline et en mettant
            en place des interventions concernant l&#8217;alimentation et
            l&#8217;activité physique, il est possible de prévenir de manière
            économique les maladies non transmissibles associées au syndrome
            métabolique
          </p>

          <div className="bgimg">
            <img src={Vision} alt="Dispositif" className="img" />
          </div>
          <p className="container dispositif-medical">
            En effet, il est important de noter que la résistance à
            l&#8217;insuline n&#8217;est pas une condamnation à vie. Si elle est
            détectée à temps, les personnes concernées possèdent une fenêtre de
            10 à 15 ans durant laquelle ils peuvent retrouver une sensibilité à
            l’insuline. Pour cela, des mesures appropriées doivent être mises en
            place, comprenant des changements de mode de vie tels qul&#8217;une
            alimentation équilibrée et une activité physique régulière.
          </p>
          <div className="bgimg">
            <img src={Dispositif} alt="Dispositif" className="img" />
          </div>

          <p className="container dispositif-medical">
            Chez Innov Biotech, notre mission est de lutter contre les maladies
            cardiométaboliques en proposant le test IDIR. Ce test offre une
            solution fiable, efficace et reproductible pour le dépistage de la
            résistance à l&#8217;insuline. En combinant la détection des
            symptômes cliniques de la résistance à l&#8217;insuline avec le test
            IDIR, nous permettons un diagnostic précis de cette condition au
            sein de la population, ainsi qu&#8217;une prévention des maladies
            cardiométaboliques. <br />
            Nous sommes déterminés à faire progresser la santé cardiovasculaire
            et métabolique, et nous espérons pouvoir contribuer à réduire
            l&#8217;impact de ces maladies à l&#8217;échelle mondiale.
            Rejoignez-nous dans notre lutte contre les maladies
            cardiométaboliques et découvrez comment notre test IDIR peut jouer
            un rôle essentiel dans la prévention et le dépistage précoce de ces
            affections.
          </p>

          <Footer />
        </>
      )}
    </>
  );
};

const sectionStyles = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};

const contentStyles = {
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  width: "100%",
};

const leftStyles = {
  flex: 1,
};

const rightStyles = {
  display: "none",
  flex: 1,
  textAlign: "right",
};
export default NotreVision;