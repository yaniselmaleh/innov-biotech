/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useState, useEffect } from "react";
import Responsive from "./responsive"; // Importez le composant responsive
import Home from "../component/home/home";
import Insuline from "../component/insuline/insuline";
import Resistance from "../component/insuline/resistance";
import Contact from "../component/contact/contact";
import Footer from "../component/footer/footer";
import Img1 from "../assets/DispositifMedical/Pictures/IDIR-SKILLCELL_New.svg";
import Img2 from "../assets/Homepage/Pictures/Scientist_1.svg";

export default function Main() {
  // Utilisez l'état pour suivre la largeur de la fenêtre
  const [windowWidth, setWindowWidth] = useState(window.innerWidth);

  useEffect(() => {
    // Fonction pour mettre à jour la largeur de la fenêtre
    const handleResize = () => {
      setWindowWidth(window.innerWidth);
    };

    // Ajoutez un écouteur d'événement pour surveiller les changements de taille de fenêtre
    window.addEventListener("resize", handleResize);

    // Retirez l'écouteur d'événement lors du démontage du composant
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  // Déterminez si la largeur de la fenêtre est inférieure à 1000px
  const isResponsive = windowWidth < 1000;

  return (
    // Utilisez un fragment pour englober le contenu
    <>
      {/* Affichez le composant Responsive si la largeur de la fenêtre est inférieure à 1000px */}
      {isResponsive && (
        <Responsive
          titre1="Des solutions de biotechnologies innovantes et accessibles"
         
          lien1="#contact"
          Img1={Img1}
          titre2="Premier test urinaire de dépistage de la résistance à l’insuline."
          texte2="Le test IDIR offre la possibilité de détecter la résistance à l’insuline chez une personne, avant même qu’elle ne développe une maladie cardio-métabolique, potentiellement grave. Facile à réaliser, ce test urinaire, non intrusif, est le premier outil adapté à une utilisation de routine et à grande échelle. Il permet un dépistage précoce de la résistance à l’insuline, jusqu’à 10 à 15 ans avant le développement de maladies liées à cette affection, les maladies dites cardiométaboliques."
          Img2={Img2}
          titre3="Pourquoi tester sa résistance à l’insuline ?"
          texte3="La résistance à l’insuline est une condition qui se produit lorsque notre corps ne répond plus correctement à l’hormone produite par le pancréas, l’insuline, qui régule le taux de glucose dans notre sang. Cette difficulté à convertir le glucose en énergie peut entraîner des risques pour notre santé, notamment le développement de maladies cardiométaboliques de maladies cardiovasculaires, comme le diabète de type 2, ou certains cancers tels que ceux du sein, du col de l’utérus, de la prostate, du côlon, de l’œsophage, du pancréas et du rein. Tester régulièrement sa résistance à l’insuline permet donc de prévenir voire empêcher l’apparition de ces pathologies. En effet, si elle est détectée à temps, la résistance à l’insuline est réversible."
        />
      )}

      {/* Affichez le reste du contenu si la largeur de la fenêtre est supérieure ou égale à 1000px */}
      {!isResponsive && (
        <>
          <Home / >
          <Insuline />
          <Resistance />
          <Contact />
          <Footer />
        </>
      )}
    </>
  );
}
