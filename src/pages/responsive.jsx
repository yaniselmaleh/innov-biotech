/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useRef } from "react";
import { slide as Menu } from "react-burger-menu";
import { Link } from "react-router-dom";
import Logo from "../assets/Elements/Logo/LogoInnov_White.svg";
import Picto from "../assets/Picto/layer1.png";
import Contact from "../component/contact/contact";
import LogoWhite from "../assets/Elements/Logo/LogoInnov_White.svg";
import Group_Scientist from "../assets/NotreDemarche/Quisommesnous/Pictures/Group_Scientist_1.svg";
import Footer1 from "../assets/Elements/Icons/Facebook.svg";
import Footer2 from "../assets/Elements/Icons/Instagram.svg";
import Footer3 from "../assets/Elements/Icons/Linkedin.svg";
import Footer4 from "../assets/Elements/Icons/Twitter.svg";

const Responsive = ({
  titre1,
  textlien1,
  lien1,
  Img1,
  titre2,
  texte2,
  axe1,
  axe2,
  axe3,
  texteBlue1,
  
  texteBlue2,
  texteBlue3,
  axe1copie,
  sousdetail1,
  sousdetail2,
  sousdetail3,
  sousdetail4,
  descAxe1,
  descAxe2,
  descAxe3,
  sousdetail5,
  sousdetail6,
  sousdetail7,
  sousdetail8,
  texteAxe1copie,
  axe2copie,
  texteAxe2copie2,
  axe3copie3,
  texteAxe3copie3,
  Img2,
  titre3,
  imageSrc,
  image1,
  image2,
  image3,
  altImage,
  imageStyle,
  desc1,
  desc2,
  desc3,
  titreidir,
  descIdir,
  texteDesc,
  texte3,
  texte4,
  texte5,
  texte6,
  texte7,
  texte8
}) => {
  const menuItems = [
    { label: "Accueil", path: "/" },
    { label: "Notre démarche", path: "/notre-demarche" },
    { label: "Notre démarche RSE", path: "/notre-demarche-rse" },
    { label: "Notre vision", path: "/notre-vision" },
    { label: "Notre politique qualité", path: "/notre-politique-qualite" },
    { label: "Dispositif médical", path: "/dispositif-medical" },
    {
      label: "Territoires d’intervention",
      path: "/territoires-d-intervention",
    },
    { label: "Conditions générales d'utilisation", path: "/cgu" },
    { label: "Contact", path: "/FHIBF" },
  ];

  const menuRef = useRef(null); // Créez une référence pour le composant Menu

  const closeMenu = () => {
    // Appel de closeMenu() pour fermer le menu
    if (menuRef.current) {
      menuRef.current.closeMenu();
    }
  };

  return (
    <section id="sectionresponsive">
      <nav className="superresponsive">
        <div className="centered-container">
          <Menu
            right
            customBurgerIcon={
              <img
                src={Picto}
                alt="menu"
                id="menuburger1"
                style={{ width: "20px" }}
              />
            }
            ref={menuRef} // Attachez la référence au composant Menu
          >
            <ul>
              <div className="centered-logo">
                <Link to="/">
                  <img
                    src={Logo}
                    alt="Logo Innov Biotech"
                    className="logoresponsive"
                  />
                </Link>
              </div>
              {menuItems.map((menuItem) => (
                <li key={menuItem.path}>
                  {menuItem.label === "Contact" ? (
                    <a href="#contact" onClick={closeMenu}>
                      {menuItem.label}
                    </a>
                  ) : (
                    <Link to={menuItem.path} onClick={closeMenu}>
                      {menuItem.label}
                    </Link>
                  )}
                </li>
              ))}
            </ul>
          </Menu>
          <div className="centered-logo">
            <Link to="/">
              <img
                src={Logo}
                alt="Logo Innov Biotech"
                className="logoresponsive"
              />
            </Link>
          </div>
        </div>
        <div className="titreresponsive">
          {!titre1 ? null : <h1>{titre1}</h1>}
          <br />
        </div>
        {/* <a href={lien1} className="bottom-link">
          {textlien1}
        </a> */}
      </nav>
      <div className="orangeresponsive" >
        <div>
          {/* <h2>{titre2}</h2> */}
          <p>{texte2}</p>
          <Link to="/dispositif-medical" className="textSavoir">En savoir plus</Link>
          <br />
          {/* <ul>
            <li style={textBlue}>{axe1}</li>
              <p>{texteAxe1}</p>
            <li style={textBlue}>{axe2}</li>
                <p>{texteAxe2}</p>
            <li style={textBlue}>{axe3}</li>
              <p>{texteAxe3}</p>
          </ul> */}
          {/* <ul>
            <li style={textBlue}>{axe1copie}</li>
              <p>
                {texteAxe1copie} <br />
                - {sousdetail1} <br />
                - {sousdetail2} <br />
                - {sousdetail3} <br />
                - {sousdetail4} <br />

          {(titre2 && texte2)&&(
            <div>
              <h2>{titre2}</h2>
              <p>{texte2}</p>
            </div>
          )}

          {(titreidir) &&(
            <div>
              <h1>{titreidir}</h1>
              <p> {descIdir} </p>
            </div>
          )}
          
          
          {imageSrc?(
              <div className="container">
                <div className="row">
                  <img src={imageSrc}
                    alt={altImage}
                    style={imageStyle}
                  />
                </div>
              </div>
            ): (
              <p></p>
            )}
          {(axe1copie && texteAxe1copie) &&(
            <ul>
              <li style={textBlue}>{axe1copie}</li>
                <p>
                  {texteAxe2copie2} <br />
                  - {sousdetail5} <br />
                  - {sousdetail6} <br />
                  - {sousdetail7} <br />
                  - {sousdetail8} <br />
                  </p>
            <li style={textBlue}>{axe3copie3}</li>
              <p>{texteAxe3copie3}</p>
          </ul> */}
    
        </div>
      </div>
      
      <div className="logoorangeresponsive1" id="detail">
        {!Img1 ? null : (
          <img
            src={Img1}
            alt="Logo"
            
          />
        )}
      </div>
      <div className="titreettxtresponsive">
        {!titre3 ? null : <h3>{titre3}</h3>}
        <hr className="hrresponsive" />
        <p>{texte3}</p>
        <div className="logoorangeresponsive"  >
          {!Img2 ? null : (
            <img src={Img2} alt="Logo" />
          )}
        </div>
       
        <p>{texte8}</p>
       
      </div>

      <section className="blocconfiance">
        <h2>Nous sommes membres des pôles de compétitivité et d’innovation</h2>
        <div>
          <div className="blocconfiancediv">
            <a
              href="https://frenchhealthcare-association.fr/"
              target="_blank"
              rel="noopener noreferrer"
            >
              <img
                src="https://www.adechotech.fr/wp-content/uploads/2016/03/FrenchHealthcareLogo.png"
                alt="Description de l&#8217;image 1"
              />
            </a>
          </div>
          <div className="blocconfiancediv">
            <a
              href="https://www.eurobiomed.org"
              target="_blank"
              rel="noopener noreferrer"
            >
              <img
                src="https://www.eurobiomed.org/wp-content/uploads/2020/12/Logo-Eurobiomed-1-2.png"
                alt="Description de l&#8217;image 2"
              />
            </a>
          </div>
          <div className="blocconfiancediv">
            <a
              href="https://www.medvallee.fr/fr"
              target="_blank"
              rel="noopener noreferrer"
            >
              <img
                src="https://www.afssi-connexions.fr/wp-content/uploads/2022/12/Medvallee_logo.png"
                alt="Description de l&#8217;image 3"
              />
            </a>
          </div>
          <div className="blocconfiancediv">
            <a
              href="https://www.africalink.fr/"
              target="_blank"
              rel="noopener noreferrer"
            >
              <img
                src="https://www.africalink.fr/logo.png"
                alt="Description de l&#8217;image 4"
              />
            </a>
          </div>
        </div>
      </section>
      <Contact />

      <footer id="footer" style={footerStyles}>
        <section className="container" style={containerStyles}>
          <div className="footer-line" style={lineStyles}>
            <div className="footer-logo">
              <img src={LogoWhite} alt="Logo" />
            </div>
            {/* <div className="footer-text" style={textStyles}>
              <div className="footer-column">
                <ul>
                  <li>
                    <a href="#" title="" target="_blank" rel="noreferrer">
                      <img src={Footer1} alt="Image RS" />
                    </a>
                  </li>
                </ul>
              </div>
              <div className="footer-column">
                <ul>
                  <li>
                    <a href="#" title="" target="_blank" rel="noreferrer">
                      <img src={Footer2} alt="Image RS" />
                    </a>
                  </li>
                </ul>
              </div>
              <div className="footer-column">
                <ul>
                  <li>
                    <a
                      href="https://www.linkedin.com/company/innovbiotech-france/?originalSubdomain=fr"
                      title=""
                      target="_blank"
                      rel="noreferrer"
                    >
                      <img src={Footer3} alt="Image RS" />
                    </a>
                  </li>
                </ul>
              </div>
              <div className="footer-column">
                <ul>
                  <li style={{ marginTop: "5vh" }}>
                    <a href="#" title="" target="_blank" rel="noreferrer">
                      <img src={Footer4} alt="Image RS" />
                    </a>
                  </li>
                </ul>
              </div>
            </div> */}
          </div>
          <p style={texteFooter}>
            Copyright © 2023 INNOV BIOTECH All rights reserved.
          </p>
        </section>
      </footer>
    </section>
  );
};

const footerStyles = {
  display: "flex",
  flexDirection: "column", // Disposition en colonnes
  alignItems: "center", // Centre les éléments horizontalement
  padding: "20px", // Ajoute un espace autour du footer
};

const texteFooter = {
  marginTop: "5vh",
  textAlign: "center",
  color: "white",
};

const containerStyles = {
  width: "100%", // Assurez-vous que le contenu du footer occupe toute la largeur
};

const lineStyles = {
  display: "flex",
  alignItems: "center", // Centre les éléments horizontalement
  justifyContent: "space-between", // Répartit les éléments à gauche et à droite
  width: "100%", // Assurez-vous que les lignes occupent toute la largeur disponible
};

const textStyles = {
  display: "flex",
  justifyContent: "space-between",
  width: "50%",
  alignItems: "center",
  margin: "auto",
  flexDirection: "row",
};
const textBlue ={
  color: "#257c9e",
}


export default Responsive;
