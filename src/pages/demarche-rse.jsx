/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useState, useEffect } from "react";
import Responsive from "./responsive";
import Header from "../component/header/header";
import Logo from "../assets/Elements/Logo/LogoInnov_White.svg";
import Scientist from "../assets/NotreDemarche/NotredemarcheRSE/Pictures/Medical.png";
import Footer from "../component/footer/footer";

const NotreDemarcheRSE = () => {
  const [windowWidth, setWindowWidth] = useState(window.innerWidth);
  useEffect(() => {
    // Fonction pour mettre à jour la largeur de la fenêtre
    const handleResize = () => {
      setWindowWidth(window.innerWidth);
    };

    // Ajoutez un écouteur d'événement pour surveiller les changements de taille de fenêtre
    window.addEventListener("resize", handleResize);

    // Retirez l'écouteur d'événement lors du démontage du composant
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  // Déterminez si la largeur de la fenêtre est inférieure à 1000px
  const isResponsive = windowWidth < 1000;

  return (
    <>
      {isResponsive && (
        <Responsive
          titre1="Notre Démarche RSE"
          textlien1="Découvrez le test IDIR"
          lien1="#contact"
          Img1={Scientist}
          altImage="Scientist"
          imageStyle={imageStyle}
          titre2="Engagés dans une démarche responsable"
          texte2="Chez Innov Biotech, nous sommes engagés dans une démarche responsable envers toutes nos parties prenantes, aussi bien internes comme externes. Notre ambition est de contribuer au mieux-être de chacun, que cela soit en contribuant à un épanouissement de nos salariés ou à l’amélioration de la santé des populations.La démarche
          RSE d’Innov Biotech est structurée autour de 3 axes :"

          axe1copie="Axe sociétal"
          texteAxe1copie="Contribuer à l’amélioration de la santé et du bien-être de chacun."

          axe2copie="Axe social"
          texteAxe2copie2="Engager les collaborateurs pour optimiser la performance de l'entreprise."

          axe3copie3="Axe sport santé"
          texteAxe3copie3="Engager à l’activité physique ou sportive pour contribuer à une meilleure santé."

          descAxe1={"L’engagement d’Innov Biotech envers une meilleure santé de façon globale se traduit par la mise en œuvre de quatre priorités qui relèvent du champ sociétal : "}
          sousdetail1="- Améliorer l’accès aux soins dans les pays où les systèmes de santé sont trop souvent fragiles ; "
          sousdetail2="- Innover pour le mieux vivre et rendre accessible les innovations de santé au plus grand nombre ; "
          sousdetail3="- Garantir la protection de leurs données aux bénéficiaires de nos solutions."
          sousdetail4="- Engager les collaborateurs pour optimiser la performance de l'entreprise."

          descAxe2="Chez Innov Biotech, nous sommes déterminés à créer un cadre de travail épanouissant et motivant, où chaque salarié peut se développer et s'épanouir. Quatre objectifs nous guident dans la mobilisation des équipes en faveur de la performance de l’entreprise :"
          sousdetail5="- Rendre les collaborateurs acteurs de la transformation de l’entreprise ;"
          sousdetail6="- Renforcer la confiance mutuelle et l’engagement ;"
          sousdetail7="- Favoriser le développement des compétences de chacun via des offres de formation ;"
          sousdetail8="- Promouvoir la diversité et l’égalité professionnelle."

          descAxe3="Innov Biotech souhaite lutter contre les mauvaises habitudes alimentaires et la sédentarité en adoptant un mode de vie sain et actif et en assurant la promotion du sport santé. Nous voulons engager un mouvement d’ampleur auprès des entreprises et du grand public pour amener les gens à bouger et changer les mentalités autour de la pratique sportive."

          // Img2=""
          // texte3="Le test IDIR est le premier test urinaire de dépistage de la résistance à l’insuline permettant un dépistage précoce et ciblé des personnes présentant un risque important de développer des maladies cardiométaboliques.Innov Biotech collabore également avec les agences de réglementation pharmaceutique pour l’obtention des autorisations de mise sur le marché (AMM) et assure la disponibilité du test. De plus, l’entreprise soutient les études de vérification et de validation du test IDIR."

        />
      )}

      {!isResponsive && (
        <>
            <div className="header-img">
    <Header  logo={Logo} / >
    </div>
          <section id="notre-demarche" style={sectionStyles}>
            <div style={contentStyles} className="container topbanniere">
              <div style={leftStyles}>
                <h1>Notre Démarche RSE</h1>
              </div>
              <div style={rightStyles}>
                <p>Notre démarche / Notre démarche RSE</p>
              </div>
            </div>
          </section>

          <section style={section2Styles} className="container">
            <div style={flexContainerStyles}>
              <div style={itemStyles}>
                <div style={textImageContainer} className="flex-columns">
                  <p style={textStyles}>
                    <h2 id="titre2">Engagés dans une démarche responsable</h2>
                    <br />
                    <p>
                      Chez Innov Biotech, nous sommes engagés dans une démarche
                      responsable envers toutes nos parties prenantes, aussi
                      bien internes comme externes. Notre ambition est de
                      contribuer au mieux-être de chacun, que cela soit en
                      contribuant à un épanouissement de nos salariés ou à
                      l’amélioration de la santé des populations. La démarche
                      RSE d’Innov Biotech est structurée autour de 3 axes :
                      <br />
                      <br />
                      <span className="orange">Axe sociétal</span>
                      <br /> Contribuer à l’amélioration de la santé et du
                      bien-être de chacun.
                      <br />
                      <br />
                      <span className="orange">Axe social</span>
                      <br />
                      Engager les collaborateurs pour optimiser la performance
                      de l&#8217;entreprise.
                      <br />
                      <br />
                      <span className="orange">Axe sport santé</span>
                      <br />
                      Engager à l’activité physique ou sportive pour contribuer
                      à une meilleure santé.
                      <br />
                      <br />
                      <span className="orange">Axe sociétal</span>
                      <br />
                      L’engagement d’Innov Biotech envers une meilleure santé de
                      façon globale se traduit par la mise en oeuvre de quatre
                      priorités qui relèvent du champ sociétal : <br />-
                      Améliorer l’accès aux soins dans les pays où les systèmes
                      de santé sont trop souvent fragiles ; <br />- Innover pour
                      le mieux vivre et rendre accessible les innovations de
                      santé au plus grand nombre ; <br />- Garantir la
                      protection de leurs données aux bénéficiaires de nos
                      solutions. <br />- Engager les collaborateurs pour
                      optimiser la performance de l&#8217;entreprise.
                      <br />
                      <br />
                      <span className="orange">Axe social</span>
                      <br />
                      Chez Innov Biotech, nous sommes déterminés à créer un
                      cadre de travail épanouissant et motivant, où chaque
                      salarié peut se développer et s&#8217;épanouir. Quatre
                      objectifs nous guident dans la mobilisation des équipes en
                      faveur de la performance de l’entreprise :<br />
                      <br />- Rendre les collaborateurs acteurs de la
                      transformation de l’entreprise ; <br />- Renforcer la
                      confiance mutuelle et l’engagement ; <br />- Favoriser le
                      développement des compétences de chacun via des offres de
                      formation ; <br />- Promouvoir la diversité et l’égalité
                      professionnelle.
                      <br />
                      <br />
                      <span className="orange">Axe sport santé</span>
                      <br />
                      santé Innov Biotech souhaite lutter contre les mauvaises
                      habitudes alimentaires et la sédentarité en adoptant un
                      mode de vie sain et actif et en assurant la promotion du
                      sport santé. Nous voulons engager un mouvement d’ampleur
                      auprès des entreprises et du grand public pour amener les
                      gens à bouger et changer les mentalités autour de la
                      pratique sportive.
                    </p>
                    <h2 id="titre2">Le premier test urinaire de dépistage de la résistance à l’insuline</h2>
                        <br />
                        Le test IDIR est le premier test urinaire de dépistage de la résistance à l’insuline permettant un dépistage précoce et ciblé des personnes présentant un risque important de développer des maladies cardiométaboliques.Innov Biotech collabore également avec les agences de réglementation pharmaceutique pour l’obtention des autorisations de mise sur le marché (AMM) et assure la disponibilité du test. De plus, l’entreprise soutient les études de vérification et de validation du test IDIR.
                  </p>
                  <img
                    src={Scientist}
                    alt="Group_Scientist"
                    style={imageStyles}
                  />
                 
                </div>
                
              </div>
            </div>
          </section>

          <Footer />
        </>
      )}
    </>
  );
};

const sectionStyles = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};



const contentStyles = {
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  width: "100%",
};

const leftStyles = {
  flex: 1,
};

const rightStyles = {
  display: "none",
  flex: 1,
  textAlign: "right",
};

const section2Styles = {
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  marginBottom: "10vh",
};

const flexContainerStyles = {
  display: "flex",
  justifyContent: "center",
  flexWrap: "wrap",
  width: "100%",
};

const itemStyles = {
  display: "flex",
  alignItems: "center",
  width: "100%",
  marginTop: "5vh",
};

const imageStyles = {
  flex: "0 0 auto",
  marginRight: "20px",
};

const imageStyle = {
  flex: "0 0 auto", // Empêche l&#8217;image de s&#8217;étirer
  height: "500px",
};

const textStyles = {
  fontSize: "18px",
  padding: "3%",
  color: "#257c9e",
  lineHeight: "1.3",
  flex: "1",
};

const textImageContainer = {
  display: "flex",
  flexDirection: "row", // Permet au texte et à l&#8217;image d&#8217;être côte à côte
  alignItems: "flex-start", // Alignement en haut de la hauteur disponible
};

export default NotreDemarcheRSE;
