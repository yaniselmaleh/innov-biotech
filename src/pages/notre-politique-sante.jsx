/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useState, useEffect } from "react";
import Responsive from "./responsive";
import Header from "../component/header/header";
import Logo from "../assets/Elements/Logo/LogoInnov_White.svg";
import Img1 from "../assets/NotreDemarche/NotredemarcheRSE/Pictures/Scientist_1.png";
import Img2 from "../assets/NotreDemarche/NotredemarcheRSE/Pictures/Scientist_2.png";
import Img3 from "../assets/NotreDemarche/NotredemarcheRSE/Pictures/Scientist_3.png";
import Footer from "../component/footer/footer";

const NotrePolitiqueQualite = () => {
  const [windowWidth, setWindowWidth] = useState(window.innerWidth);
  useEffect(() => {
    // Fonction pour mettre à jour la largeur de la fenêtre
    const handleResize = () => {
      setWindowWidth(window.innerWidth);
    };

    // Ajoutez un écouteur d'événement pour surveiller les changements de taille de fenêtre
    window.addEventListener("resize", handleResize);

    // Retirez l'écouteur d'événement lors du démontage du composant
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  // Déterminez si la largeur de la fenêtre est inférieure à 1000px
  const isResponsive = windowWidth < 1000;
  return (
    <>
      {isResponsive && (
        <Responsive
          titre1="NOTRE POLITIQUE QUALITÉ"
          textlien1="Découvrez le test IDIR"
          lien1="#contact"
          Img1={Img1}

          texte2="Chez Innov Biotech, nous sommes résolument engagés envers la qualité et la sécurité des produits que nous proposons. Nous sommes fiers de commercialiser Le test IDIR , qui répond aux exigences de la Haute Autorité de Santé (HAS), a obtenu le marquage CE et est enregistré à l’ANSM, attestant de sa conformité aux normes européennes les plus strictes."

          image1={Img1}
          desc1="Nous nous engageons à fournir des produits d'une qualité irréprochable, basés sur des avancées scientifiques et technologiques de pointe. Notre équipe de chercheurs, celle de nos partenaires et fournisseurs sont composées d'experts reconnus dans le domaine de la santé public et privé, travaille sans relâche pour développer des solutions innovantes et fiables. Avec nos partenaires et fournisseurs, nous mettons en œuvre des procédures rigoureuses pour garantir la précision et la fiabilité de nos tests, ainsi que pour assurer la traçabilité de chaque étape du processus."


          image2={Img2}
          desc2="En tant qu'acteur responsable, nous respectons strictement les normes et les réglementations en vigueur. L'obtention de l'autorisation par l’ANSM (Agence nationale de sécurité du médicament et des produits de santé) et du marquage CE pour notre test IDIR® atteste de notre engagement envers la conformité et la sécurité des produits que nous proposons. Le référentiel de certification élaboré par l’ANSM définit les normes et les critères que les entreprises du médicament doivent respecter pour garantir la qualité, la sécurité et l'efficacité de leurs produits. Nous nous engageons à remplir ces exigences avec le plus grand soin et à maintenir notre conformité en permanence."
          
          image3={Img3}
          desc3 = "Nous plaçons nos clients au cœur de nos préoccupations. Nous nous engageons à fournir un service client exceptionnel et à répondre à leurs besoins de manière efficace et réactive. Nous attachons une grande importance aux retours de nos clients et nous utilisons ces informations pour améliorer continuellement nos produits et nos processus. La satisfaction de nos clients est notre priorité absolue, et nous nous efforçons d'établir des relations de confiance à long terme."
          imageStyle={imageStyle}

        />
      )}

      {!isResponsive && (
        <>
           <div className="header-img">
    <Header  logo={Logo} / >
    </div>
          <section id="notre-demarche" style={sectionStyles}>
            <div style={contentStyles} className="container topbanniere">
              <div style={leftStyles}>
                <h1>NOTRE POLITIQUE QUALITÉ</h1>
              </div>
              <div style={rightStyles}>
                <p>Notre démarche / Notre politique qualité</p>
              </div>
            </div>
          </section>

          <h2 id="titre2" className="container">
            ENGAGEMENT ENVERS L’Excellence, LA sécurité et la satisfaction
            clients
          </h2>
          <p className="container dispositif-medical">
            Chez Innov Biotech, nous sommes résolument engagés envers la qualité
            et la sécurité des produits que nous proposons. Nous sommes fiers de
            commercialiser Le test IDIR , qui répond aux exigences de la Haute
            Autorité de Santé (HAS), a obtenu le marquage CE et est enregistré à
            l’ANSM, attestant de sa conformité aux normes européennes les plus
            strictes.
            <br />
            <br />
            Notre politique qualité repose sur trois piliers fondamentaux :
            <br />
          </p>

          <div
            className="container"
            style={flexContainerStyles}
            id="politiquesante"
          >
            <div
              className="column"
              style={{ ...flexItemStyles, textAlign: "left" }}
            >
              <img src={Img1} alt="" style={imgStyles} />
              <h2>1.</h2>
              <p>Excellence scientifique et technologique</p>
              <p>
                Nous nous engageons à fournir des produits d&#8217;une qualité
                irréprochable, basés sur des avancées scientifiques et
                technologiques de pointe. Notre équipe de chercheurs, celle de
                nos partenaires et fournisseurs sont composées d&#8217;experts
                reconnus dans le domaine de la santé public et privé, travaille
                sans relâche pour développer des solutions innovantes et
                fiables. Avec nos partenaires et fournisseurs, nous mettons en
                œuvre des procédures rigoureuses pour garantir la précision et
                la fiabilité de nos tests, ainsi que pour assurer la traçabilité
                de chaque étape du processus.
              </p>
            </div>
            <div
              className="column"
              style={{ ...flexItemStyles, textAlign: "left" }}
            >
              <img src={Img2} alt="" style={imgStyles} />
              <h2>2.</h2>
              <p>Conformité aux normes et réglementations</p>
              <p>
                En tant qu&#8217;acteur responsable, nous respectons strictement
                les normes et les réglementations en vigueur. L&#8217;obtention
                de l&#8217;autorisation par l’ANSM (Agence nationale de sécurité
                du médicament et des produits de santé) et du marquage CE pour
                notre test IDIR atteste de notre engagement envers la
                conformité et la sécurité des produits que nous proposons. Le
                référentiel de certification élaboré par l’ANSM définit les
                normes et les critères que les entreprises du médicament doivent
                respecter pour garantir la qualité, la sécurité et
                l&#8217;efficacité de leurs produits. Nous nous engageons à
                remplir ces exigences avec le plus grand soin et à maintenir
                notre conformité en permanence.
              </p>
            </div>
            <div
              className="column"
              style={{ ...flexItemStyles, textAlign: "left" }}
            >
              <img src={Img3} alt="" style={imgStyles} />
              <h2>3.</h2>
              <p>Satisfaction des clients et amélioration continue</p>
              <p>
                Nous plaçons nos clients au cœur de nos préoccupations. Nous
                nous engageons à fournir un service client exceptionnel et à
                répondre à leurs besoins de manière efficace et réactive. Nous
                attachons une grande importance aux retours de nos clients et
                nous utilisons ces informations pour améliorer continuellement
                nos produits et nos processus. La satisfaction de nos clients
                est notre priorité absolue, et nous nous efforçons
                d&#8217;établir des relations de confiance à long terme avec
              </p>
            </div>
          </div>

          <Footer />
        </>
      )}
    </>
  );
};

const sectionStyles = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
  padding: "20px",
};

const contentStyles = {
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  width: "100%",
};

const leftStyles = {
  flex: 1,
};

const flexContainerStyles = {
  display: "flex",
  justifyContent: "space-between",
};

const flexItemStyles = {
  flex: "0 0 calc(33.33% - 20px)",
};

const imgStyles = {
  maxWidth: "100%",
};
const imageStyle = {
  display:"flex",
  flexWrap: "wrap",
  justifyContent: "center",
  margin: "auto",

  // Empêche l&#8217;image de s&#8217;étirer
  height: "200px",
};

const rightStyles = {
  display: "none",
  flex: 1,
  textAlign: "right",
};

export default NotrePolitiqueQualite;
