import Header from "../../component/header/header";
import Logo from "../../assets/Elements/Logo/LogoInnov.svg";
import Img1 from "../../assets/Territoires-d-intervention/Map/Senegal.svg";
import Footer from "../../component/footer/footer";

const Senegel = () => {
  return (
    <>
      <Header logo={Logo} />
      <section id="notre-demarche" style={sectionStyles}>
        <div style={contentStyles} className="container topbanniere">
          <div style={leftStyles}>
            <h1>Sénégal</h1>
          </div>
          <div style={rightStyles}>
            <p>Nos territoires d’intervention / Sénégal</p>
          </div>
        </div>
      </section>

      <h2 className="container" id="titre2">
        Notre objectif : enrayer l’épidémie mondiale
        <br /> causée par les maladies cardiométaboliques
      </h2>
      <p className="container dispositif-medical">
        Le Sénégal, Etat d’Afrique de l’Ouest, possède un système de santé
        émergeant mais encore très fragile avec un indice de performance de 57,3
        (sur une échelle de 1 à 100), indice assez faible. En effet, le Sénégal
        manque de professionnels de santé avec 0,6 médecins pour 10 000
        habitants. De plus, leur répartition est inégale avec une forte
        concentration dans les grandes villes. L’Etat consacre 8% de son budget
        au financement de la santé. Malgré un plan d’action national intégré sur
        les maladies non transmissibles, il n’existe que peu de mesures pour
        combattre les régimes alimentaires nocifs pour la santé ou des campagnes
        de sensibilisation à l’activité physique.
      </p>

      <section id="territoire-orange">
        <div className="container" id="imgsenegal">
          <img src={Img1} alt="Sénegal" />
        </div>
        <div style={rowStyles} className="container">
          <div>
            <h3>17,9 millions</h3>
            <p>d’habitants / Espérance de vie 70 ans</p>
          </div>
          <div>
            <h3>14 108 décès</h3>
            <p>
              causés par les maladies
              <br />
              cardio-vasculaires par an
            </p>
          </div>
        </div>
        <div style={rowStyles} className="container">
          <div>
            <h3>48% de décès</h3>
            <p>
              prématurés sont causés par une <br />
              maladie cardio-vasculaire
            </p>
          </div>
          <div>
            <h3>9%</h3>
            <p>
              Prévalence de l’obésité
              <br />
              des adultes (18+)
            </p>
          </div>
        </div>
        <div style={rowStyles} className="container">
          <div>
            <h3>2,8%</h3>
            <p>
              Augmentation annuelle de
              <br />
              l’obésité adulte
            </p>
          </div>
          <div>
            <h3>3,1%</h3>
            <p>Prévalence du diabète chez les adultes</p>
          </div>
        </div>
        <div style={rowStyles} className="container">
          <div>
            <h3>3 089 décès</h3>
            <p>causés par le diabète par an</p>
          </div>
          <div>
            <h3>53% des décès</h3>
            <p>prématurés (avant 70 ans) sont causés par le diabète</p>
          </div>
        </div>
        <div style={rowStyles} className="container">
          <div>
            <h3>57,6%</h3>
            <p>
              des diabétiques ne
              <br />
              sont pas dépistés
            </p>
          </div>
          <div>
            <h3>41%</h3>
            <p>
              de la population adulte (30-79) sénégalaise,
              <br />
              touché par l’hypertension, un facteur
              <br />
              de risque prépondérant
            </p>
          </div>
        </div>
      </section>
      <Footer />
    </>
  );
};

const sectionStyles = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};

const contentStyles = {
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  width: "100%",
};

const leftStyles = {
  flex: 1,
};

const rightStyles = {
  display: "none",
  flex: 1,
  textAlign: "right",
};

const rowStyles = {
  display: "flex",
  justifyContent: "space-between", // To create space between items in a row
  alignItems: "center",
};

export default Senegel;
