/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useState, useEffect } from "react";
import Responsive from "./responsive";
import Header from "../component/header/header";
import Logo from "../assets/Elements/Logo/LogoInnov_White.svg";
import Footer from "../component/footer/footer";
import Img1 from "../assets/Territoires-d-intervention/Map/Senegal.svg";
import Img2 from "../assets/Territoires-d-intervention/Map/France.svg";
import Img3 from "../assets/Territoires-d-intervention/Map/CoteDIvoire.svg";
import { Link } from "react-router-dom";

const TerritoiresIntervention = () => {
  const [windowWidth, setWindowWidth] = useState(window.innerWidth);
  useEffect(() => {
    // Fonction pour mettre à jour la largeur de la fenêtre
    const handleResize = () => {
      setWindowWidth(window.innerWidth);
    };

    // Ajoutez un écouteur d'événement pour surveiller les changements de taille de fenêtre
    window.addEventListener("resize", handleResize);

    // Retirez l'écouteur d'événement lors du démontage du composant
    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  // Déterminez si la largeur de la fenêtre est inférieure à 1000px
  const isResponsive = windowWidth < 1000;
  return (
    <>
      {isResponsive && (
        <Responsive
          titre1="NOS TERRITOIRS D’INTERVENTION"
          textlien1="Découvrez le test IDIR"
          lien1="#contact"
          Img1={Img1}
          titre2="Innov Biotech est le distributeur du test IDIR"
          texte2="Innov Biotech est le distributeur du test IDIR, le premier test de
          dépistage de la résistance à l’insuline et se consacre aussi à la
          production et la commercialisation du test IDIR. Innov Biotech est
          le distributeur exclusif sur l’ensemble du territoire africain car
          c’est une zone de priorité absolue et collabore avec des revendeurs.
          Concernant les autres continents elle collabore avec des
          distributeurs locaux."
          Img2=""
          titre3="Le premier test urinaire de dépistage de la résistance à l’insuline"
          texte3="Les populations de nombreux pays africains sont durement touchées
          par la résistance à l&#8217;insuline et les conséquences graves des
          maladies cardiométaboliques qui en découlent. La proportion de
          diabète non diagnostiqué est la plus élevée dans la région Afrique,
          avec 53,6 %. Les maladies cardiovasculaires constituent un fléau
          encore plus important. Les AVC tuent chaque minute 40 personnes en
          Afrique. Il y a donc urgence à agir. 
          Notre objectif est de travailler en étroite collaboration avec les
          autorités sanitaires, les professionnels de la santé publics et
          privés et les communautés locales pour renforcer les capacités de
          diagnostic, sensibiliser et mettre en œuvre des stratégies
          d’interventions efficaces. Mais notre action ne se limite pas à
          l&#8217;Afrique.
          Nous avons pour ambition d’étendre notre présence et notre
          distribution à d&#8217;autres pays à travers le monde, afin de
          toucher un plus grand nombre de personnes confrontées à ces
          problématiques de santé majeures.
          Notre mission est de fournir des outils de dépistage précoce et des
          solutions adaptées pour combattre la résistance à l&#8217;insuline
          et améliorer la santé cardiométabolique des populations, où
          qu&#8217;elles se trouvent. C’est en ce sens que Innov Biotech est
          partenaire du programme PREDIA développé par l’ONG AcSan et reste
          ouvert au développement d’autres partenariats avec des ONG,
          associations, organisations de santé, …"
        />
      )}

      {!isResponsive && (
        <>
            <div className="header-img">
    <Header  logo={Logo} / >
    </div>
          <section id="notre-demarche" style={sectionStyles}>
            <div style={contentStyles} className="container topbanniere">
              <div style={leftStyles}>
                <h1>NOS TERRITOIRS D’INTERVENTION</h1>
              </div>
              <div style={rightStyles}>
                <p>Nos territoires d’intervention</p>
              </div>
            </div>
          </section>

          <p className="container dispositif-medical">
            Innov Biotech est le distributeur du test IDIR, le premier test de
            dépistage de la résistance à l’insuline et se consacre aussi à la
            production et la commercialisation du test IDIR. Innov Biotech est
            le distributeur exclusif sur l’ensemble du territoire africain car
            c’est une zone de priorité absolue et collabore avec des revendeurs.
            Concernant les autres continents elle collabore avec des
            distributeurs locaux. <br />
            <br />
            <b>
              Pour devenir distributeur ou revendeur adressez un mail à{" "}
            </b>{" "}
            <a href="mailto:contact@innovbiotech.co?subject=Demande%20de%20contact%20-%20innovbiotech">
              contact@innovbiotech.co
            </a>
            <br />
            <br />
            Les populations de nombreux pays africains sont durement touchées
            par la résistance à l&#8217;insuline et les conséquences graves des
            maladies cardiométaboliques qui en découlent. La proportion de
            diabète non diagnostiqué est la plus élevée dans la région Afrique,
            avec 53,6 %. Les maladies cardiovasculaires constituent un fléau
            encore plus important. Les AVC tuent chaque minute 40 personnes en
            Afrique. Il y a donc urgence à agir. <br />
            <br />
            Notre objectif est de travailler en étroite collaboration avec les
            autorités sanitaires, les professionnels de la santé publics et
            privés et les communautés locales pour renforcer les capacités de
            diagnostic, sensibiliser et mettre en œuvre des stratégies
            d’interventions efficaces. Mais notre action ne se limite pas à
            l&#8217;Afrique.
            <br />
            <br />
            Nous avons pour ambition d’étendre notre présence et notre
            distribution à d&#8217;autres pays à travers le monde, afin de
            toucher un plus grand nombre de personnes confrontées à ces
            problématiques de santé majeures.
            <br />
            <br />
            Notre mission est de fournir des outils de dépistage précoce et des
            solutions adaptées pour combattre la résistance à l&#8217;insuline
            et améliorer la santé cardiométabolique des populations, où
            qu&#8217;elles se trouvent. C’est en ce sens que Innov Biotech est
            partenaire du programme PREDIA développé par l’ONG AcSan et reste
            ouvert au développement d’autres partenariats avec des ONG,
            associations, organisations de santé, …
          </p>

          <h2 className="container" id="intervention">
            Découvrez nos
            <br />
            territoires d’intervention
          </h2>

          <div className="container" id="mapterritoire">
           
            <div style={flexContainerStyles}>
              <div style={itemStyles}>
                <Link to="/territoire-senegal" title="redirection">
                  <img src={Img1} alt="Sénegal" />
                  <h3>Sénegal</h3>
                </Link>
              </div>
              <div style={itemStyles}>
                <Link to="/france">
                  <img src={Img2} alt="France" />
                  <h3>France</h3>
                </Link>
              </div>
              <div style={itemStyles}>
                <Link to="/cote-divoire">
                  <img src={Img3} alt="Côte d&#8217;Ivoire" />
                  <h3>Côte d&#8217;Ivoire</h3>
                </Link>
              </div>
            </div>
          </div>

          <Footer />
        </>
      )}
    </>
  );
};

const sectionStyles = {
  display: "flex",
  justifyContent: "center",
  alignItems: "center",
};

const contentStyles = {
  display: "flex",
  flexDirection: "row",
  alignItems: "center",
  width: "100%",
};

const leftStyles = {
  flex: 1,
};

const rightStyles = {
  display: "none",
  flex: 1,
  textAlign: "right",
};

const flexContainerStyles = {
  display: "flex",
  justifyContent: "space-around", // Centrer horizontalement les éléments enfants
  alignItems: "center", // Centrer verticalement les éléments enfants
  marginTop: "5vh",
  marginBottom: "5vh",
};

const itemStyles = {
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  textAlign: "center",
  margin: "0 20px", // Espace entre chaque élément
};

export default TerritoiresIntervention;
