/* eslint-disable no-unused-vars */
import React, { lazy, Suspense } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import NotFound from "./router/404.jsx";
import "./App.css";
import ContactVision from "./pages/contact-visite";
const Home = lazy(() => import("./pages/main.jsx"));
const NotreDemarche = lazy(() => import("./pages/notre-demarche"));
const NotreDemarcheRSE = lazy(() => import("./pages/demarche-rse"));
const NotrePolitiqueQualite = lazy(() =>
  import("./pages/notre-politique-sante"),
);
const TestIdir = lazy(()=> import("./pages/test-idir.jsx"))
const DispositifMedical = lazy(() => import("./pages/dispositif-medical.jsx"));
const TerritoiresIntervention = lazy(() =>
  import("./pages/territoires-d-intervention"),
);
const ReferencesPartenaires = lazy(() =>
  import("./pages/references-partenaires.jsx"),
);
const NotreVision = lazy(() => import("./pages/notre-vision"));
const Senegal = lazy(() => import("./pages/territoire/senegal"));
const QuiSommesNous = lazy(() => import("./pages/qui-sommes-nous"));
const CGU = lazy(() => import("./pages/cgu.jsx"));

// const NousRejoindre = lazy(() => import("./pages/nous-rejoindre"));
export default function App() {
  return (
    <Router>
      <Suspense fallback={<div>Loading...</div>}>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/notre-demarche" component={NotreDemarche} />
          <Route path="/notre-demarche-rse" component={NotreDemarcheRSE} />
          <Route
            path="/notre-politique-qualite"
            component={NotrePolitiqueQualite}
          />
          <Route path="/dispositif-medical" component={DispositifMedical} />
          <Route
            path="/territoires-d-intervention"
            component={TerritoiresIntervention}
          />
          <Route
            path="/references-et-partenaires"
            component={ReferencesPartenaires}
          />
          <Route path="/notre-vision" component={NotreVision} />
          <Route path="/territoire-senegal" component={Senegal} />
          <Route path="/qui-sommes-nous" component={QuiSommesNous} />
          <Route path="/FHIBF" component={ContactVision} />
          <Route path="/cgu" component={CGU} />
          <Route path="/le-test-idir" component={TestIdir} />
          {/* <Route path="/nous-rejoindre" component={NousRejoindre} /> */}
          <Route component={NotFound} />
        </Switch>
      </Suspense>
    </Router>
  );
}
