import Header from "../header/header";
import Logo from "../../assets/Elements/Logo/LogoInnov_White.svg";

const Home = () => {
  return (
    <div id="home" className="home-container">
      <Header logo={Logo} />
      <div className="container">
        <h1 className="main-heading">
          Des solutions
          <br /> de biotechnologies
          <br /> innovantes et accessibles
        </h1>
        
      </div>
    </div>
  );
};

export default Home;
