/* eslint-disable no-unused-vars */
import React, { useState } from "react";
import { Link, useLocation } from "react-router-dom";
import PropTypes from "prop-types";

const Header = ({ logo }) => {
  const [showSubMenu, setShowSubMenu] = useState(false);
  const location = useLocation();

  const handleMenuItemHover = (show) => {
    setShowSubMenu(show);
  };

  const activeStyle = {
    color: "#e2762d",
  };
  const inactiveStyle = {
    color: "#257c9e",
  };

  const menuItems = [
    { label: "Accueil", path: "/" },
    { label: "Qui sommes-nous ?", path: "/notre-demarche" },
    { label: "Dispositif médical", path: "/dispositif-medical" },
    { label: "Aires thérapeutiques", path: "/notre-vision" },
    {
      label: "Territoires d’intervention",
      path: "/territoires-d-intervention",
    },
    { label: "Ils nous font confiance", path: "/references-et-partenaires" },
    { label: "Contact", path: "/FHIBF" },
  ];

  return (
    <header>
      <section style={headerStyles} className="container">
        <div>
          <Link to="/">
            <img src={logo} style={zIndex_img} alt="Logo Innov Biotech" />
          </Link>
        </div>
        <div className="cta">
          <a href="#contact" title="Redirection vers la section Contact">
            Tester votre insulino résistance
          </a>
        </div>
      </section>
      <section className="container" id="header">
        <ul style={listStyles}>
          {menuItems.map((menuItem) => (
            <li
              key={menuItem.path}
              onMouseEnter={() =>
                handleMenuItemHover(menuItem.path === "/notre-demarche")
              }
              onMouseLeave={() => handleMenuItemHover(false)}
            >
              <Link
                to={menuItem.path}
                style={
                  location.pathname === menuItem.path || showSubMenu
                    ? activeStyle
                    : inactiveStyle
                }
              >
                {menuItem.label}
              </Link>
              {menuItem.path === "/notre-demarche" && showSubMenu && (
                <ul style={subMenuStyles}>
                  <li style={subMenuStylesListe}>
                    <Link to="/notre-vision">Notre vision</Link>
                  </li>
                  <li style={subMenuStylesListe}>
                    <Link to="/notre-demarche-rse">Notre démarche RSE</Link>
                  </li>
                  <li style={subMenuStylesListe}>
                    <Link to="/notre-politique-qualite">
                      Notre politique qualité
                    </Link>
                  </li>
                </ul>
              )}
            </li>
          ))}
        </ul>
      </section>
    </header>
  );
};

Header.propTypes = {
  logo: PropTypes.string.isRequired,
};
Header.propTypes = {
  background: PropTypes.string,
};
const headerStyles = {
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
  padding: "30px",
};

const listStyles = {
  listStyle: "none",
  display: "flex",
  justifyContent: "space-between",
  background: "white",
  color: "#257c9e",
  padding: "2% 3%",
  borderRadius: "5px",
  boxShadow: "0px 2px 5px rgba(0, 0, 0, 0.2)",
};

const subMenuStyles = {
  position: "absolute",
  width: "30%",
  top: "5vh",
  left: "0",
  background: "white",
  listStyle: "none",
  margin: "0 0",
  padding: "0 2% 2% 2%",
  transitionDuration: ".5s",
};

const subMenuStylesListe = {
  marginBottom: "1vh",
};

const zIndex_img={
zIndex:"3000"
}
export default Header;
