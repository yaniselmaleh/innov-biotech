/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useRef } from "react";
import emailjs from "@emailjs/browser";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

const Contact = () => {
  const form = useRef();
  const sendEmail = (e) => {
    e.preventDefault();

    emailjs
      .sendForm(
        "service_3nmjn2a",
        "template_ror5ivl",
        form.current,
        "ZJRvQtAmv__lLFzZH",
      )
      .then(
        (result) => {
          console.log(result.text);
          toast.success("Votre message a été envoyé avec succès");
        },
        (error) => {
          console.log(error.text);
        },
      );
  };
  return (
    <div id="contact">
      <h2 className="container">Nous contacter</h2>
      <ToastContainer />
      <form
        ref={form}
        onSubmit={sendEmail}
        className="container"
        style={containerStyles}
      >
        <div style={leftColumnStyles}>
          <div className="form-group">
            <label htmlFor="nom" style={labelFrom}>
              Nom
            </label>
            <input
              type="text"
              id="nom"
              name="nom"
              style={inputStyles}
              
            />
          </div>
          <div className="form-group">
            <label htmlFor="prenom" style={labelFrom}>
              Prénom
            </label>
            <input
              type="text"
              id="prenom"
              name="prenom"
              style={inputStyles}
             
            />
          </div>
          <div className="form-group">
            <label htmlFor="ville" style={labelFrom}>
              Ville
            </label>
            <input
              type="text"
              id="ville"
              name="ville"
              style={inputStyles}
              
            />
          </div>
          <div className="form-group">
            <label htmlFor="pays" style={labelFrom}>
              Pays
            </label>
            <input
              type="text"
              id="pays"
              name="pays"
              style={inputStyles}
              
            />
          </div>
          <br />
          <div className="form-group">
            <label htmlFor="email" style={labelFrom}>
              Email
            </label>
            <input
              type="email"
              id="email"
              name="email"
              style={inputStyles}
             
            />
          </div>
        </div>
        <div style={rightColumnStyles}>
          <div className="form-group">
            <label htmlFor="message" style={labelFrom}>
              Votre message
            </label>
            <textarea
              id="message"
              name="message"
              rows="15"
              style={inputStyles}
             
            ></textarea>
          </div>
          <div className="form-group" style={submitStyles}>
            <button type="submit" id="envoyer" style={ButtonStyles}>
              ENVOYER
            </button>
          </div>
        </div>
      </form>
    </div>
  );
};

const containerStyles = {
  display: "flex",
  justifyContent: "space-between", // Répartit les colonnes à gauche et à droite
};

const leftColumnStyles = {
  flex: "0 0 48%", // Utilisez la flexibilité pour les champs de gauche
};

const rightColumnStyles = {
  flex: "0 0 48%", // Utilisez la flexibilité pour le champ de droite
};

const inputStyles = {
  padding: "20px", // Ajoute du padding aux champs d'entrée
  width: "100%", // Assurez-vous que les champs d'entrée prennent toute la largeur disponible
};

const submitStyles = {
  textAlign: "right", // Aligne le bouton à droite
  marginTop: "20px", // Ajoute un espace au-dessus du bouton
};

const labelFrom = {
  marginTop: "2vh",
  display: "block",
};
const ButtonStyles={
height:"60px",
width:"20vh",
borderRadius:"10px"
}
export default Contact;

{
  /* <form ref={form} onSubmit={sendEmail}>
        <label>Name</label>
        <input type="text" name="name" />
        <label>Email</label>
        <input type="email" name="email" />
        <label>Message</label>
        <textarea name="message" />
        <button type="submit">Envoyer</button>
      </form> */
}
