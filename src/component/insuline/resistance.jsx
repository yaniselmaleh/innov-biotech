import InsulineImg from "../../assets/Homepage/Pictures/Scientist_1.svg";
// import { Link } from "react-router-dom";

const Resistance = () => {
  return (
    <div id="resistance">
      <section className="container" style={containerStyles}>
        <div style={imageStyles}>
          <img src={InsulineImg} alt="Insuline" />
        </div>
        <div>
          <h2>
            Pourquoi tester sa <br />
            résistance à l’insuline ?
          </h2>
          <hr />
          <p>
            La résistance à l’insuline est une condition qui se produit lorsque
            notre corps ne répond plus correctement à l&#8217;hormone produite
            par le pancréas, l’insuline, qui régule le taux de glucose dans
            notre sang. Cette difficulté à convertir le glucose en énergie peut
            entraîner des risques pour notre santé, notamment le développement
            de maladies cardiométaboliques de maladies cardiovasculaires, comme
            le diabète de type 2, ou certains cancers tels que ceux du sein, du
            col de l&#8217;utérus, de la prostate, du côlon, de
            l&#8217;œsophage, du pancréas et du rein. Tester régulièrement sa
            résistance à l’insuline permet donc de prévenir voire empêcher
            l’apparition de ces pathologies. En effet, si elle est détectée à
            temps, la résistance à l’insuline est réversible.
          </p>

          {/* <Link to="/territoire-senegal" title="redirection" className="cta">
            DECOUVREZ L’ENSEMBLE DE NOS AIRES THERAPEUTIQUES
          </Link> */}
        </div>
      </section>
    </div>
  );
};

const containerStyles = {
  display: "flex",
  flexDirection: "row", // Utilisez "row" pour que les divs soient côte à côte
  alignItems: "center", // Centre les éléments horizontalement
};

const imageStyles = {
  flex: "0 0 auto", // Empêche l&#8217;image de s&#8217;étendre en cas de peu de contenu
};

export default Resistance;
