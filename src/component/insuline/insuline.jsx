import { Link } from "react-router-dom";
import InsulineImg from "../../assets/DispositifMedical/Pictures/IDIR-SKILLCELL_New.svg";

const Insuline = () => {
  return (
    <div id="insuline" className="container" style={containerStyles}>
      <div style={textStyles}>
        <h2>
          Premier test urinaire de dépistage de la résistance à l’insuline.
        </h2>

        <p>
          Le test IDIR offre la possibilité de détecter la résistance à
          l’insuline chez une personne, avant même qu’elle ne développe une
          maladie cardio-métabolique, potentiellement grave. Facile à réaliser,
          ce test urinaire, non intrusif, est le premier outil adapté à une
          utilisation de routine et à grande échelle. Il permet un dépistage
          précoce de la résistance à l’insuline, jusqu’à 10 à 15 ans avant le
          développement de maladies liées à cette affection, les maladies dites
          cardiométaboliques.
        </p>

        <Link to="/dispositif-medical">En savoir plus</Link>
      </div>
      <div style={imageStyles}>
        <img src={InsulineImg} alt="Insuline" />
      </div>
    </div>
  );
};

const containerStyles = {
  display: "flex",
  alignItems: "center", // Centre les éléments verticalement
};

const textStyles = {
  background: "#e2762d",
  padding: "5% 3% 5% 3%",
  color: "white",
  flex: 1, // Fait en sorte que le texte prenne tout l'espace restant
  marginRight: "20px", // Espace entre le texte et l'image (ajustez selon vos besoins)
};

const imageStyles = {
  flex: 1, // Fait en sorte que l'image prenne tout l'espace restant
};

export default Insuline;
