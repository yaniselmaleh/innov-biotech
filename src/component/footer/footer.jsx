/* eslint-disable no-unused-vars */
import React from "react";
import { Link } from "react-router-dom"; // Importez le composant Link depuis React Router
import LogoWhite from "../../assets/Elements/Logo/LogoInnov_White.svg";
import Contact from "../../component/contact/contact";
const Footer = () => {
  return (
    <><section className="blocconfiance">
      <h2>Nous sommes membres des pôles de compétitivité et d’innovation</h2>
      <div className="blocconfiancedivDesktop">
       
          <a
            href="https://frenchhealthcare-association.fr/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <img
              src="https://www.adechotech.fr/wp-content/uploads/2016/03/FrenchHealthcareLogo.png"
              alt="Description de l&#8217;image 1" />
          </a>
        
          <a
            href="https://www.eurobiomed.org"
            target="_blank"
            rel="noopener noreferrer"
          >
            <img
              src="https://www.eurobiomed.org/wp-content/uploads/2020/12/Logo-Eurobiomed-1-2.png"
              alt="Description de l&#8217;image 2" />
          </a>
       
          <a
            href="https://www.medvallee.fr/fr"
            target="_blank"
            rel="noopener noreferrer"
          >
            <img
              src="https://www.afssi-connexions.fr/wp-content/uploads/2022/12/Medvallee_logo.png"
              alt="Description de l&#8217;image 3" />
          </a>
       
          <a
            href="https://www.africalink.fr/"
            target="_blank"
            rel="noopener noreferrer"
          >
            <img
              src="https://www.africalink.fr/logo.png"
              alt="Description de l&#8217;image 4" />
          </a>
      </div>
    </section><Contact /><footer id="footer" style={footerStyles}>
        <section className="container" style={containerStyles}>
          <div className="footer-line" style={lineStyles}>
            <div className="footer-logo">
              <img src={LogoWhite} alt="Logo" />
            </div>

          </div>
          <p style={texteFooter}>
            Copyright © 2023 INNOV BIOTECH All rights reserved.
          </p>
        </section>
      </footer></>
    
  );
};


const footerStyles = {
  display: "flex",
  flexDirection: "column", // Disposition en colonnes
  alignItems: "center", // Centre les éléments horizontalement
  padding: "20px", // Ajoute un espace autour du footer
};

const texteFooter = {
  marginTop: "5vh",
  textAlign: "center",
  color: "white",
};


const containerStyles = {
  width: "100%", // Assurez-vous que le contenu du footer occupe toute la largeur
};

const lineStyles = {
  display: "flex",
  alignItems: "center", // Centre les éléments horizontalement
  justifyContent: "center", // Répartit les éléments à gauche et à droite
  width: "100%", // Assurez-vous que les lignes occupent toute la largeur disponible
};

const textStyles = {
  display: "flex",
  justifyContent: "space-evenly",
  flex: 1, // La colonne de texte prend tout l&#8217;espace disponible
};

export default Footer;
