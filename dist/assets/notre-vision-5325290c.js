import{r as t,j as e,H as o,L as d,F as u}from"./index-a4e2e22d.js";import{R as c}from"./responsive-118b08d8.js";import{D as i}from"./Schema_Notre_Vision_v2-bae3aab4.js";const a="/assets/Schéma_Notre_Vision-759962fb.png",h=()=>{const[l,r]=t.useState(window.innerWidth);t.useEffect(()=>{const n=()=>{r(window.innerWidth)};return window.addEventListener("resize",n),()=>{window.removeEventListener("resize",n)}},[]);const s=l<1e3;return e.jsxs(e.Fragment,{children:[s&&e.jsx(c,{titre1:"NOTRE VISION",textlien1:"Découvrez le test IDIR",lien1:"#contact",Img1:a,titre2:"NOTRE OBJECTIF : ENRAYER L’ÉPIDÉMIE MONDIALE CAUSÉE PAR LES MALADIES CARDIOMÉTABOLIQUES",texte2:"Selon le rapport 2022 de l’OMS (Organisation mondiale de la santé) intitulé 'Invisible numbers', les maladies non transmissibles, telles que les maladies cardiovasculaires et le diabète de type 2, représentent plus de 70% des décès dans le monde. Ces maladies ont des causes multiples, à la fois sociales, environnementales, commerciales et génétiques, et touchent toutes les régions du globe. Malheureusement, chaque année, 17 millions de personnes de moins de 70 ans meurent prématurément des suites de ces maladies.",Img2:i,titre3:`Cependant, bon nombre de ces décès prématurés pourraient être évités. Pour cela il est primordial de dépister les maladies
          cardiométaboliques (diabètes…) et de se concentrer sur la réduction
          des facteurs de risque associés à ces maladies, tels qu’une
          mauvaise alimentation et la sédentarité. Heureusement, des solutions
          peu coûteuses existent pour les gouvernements et les autres acteurs
          impliqués, afin de réduire ces facteurs de risque modifiables, tel
          que le programme PREDIA proposé par l’ONG AcSan.`,texte3:`Parmi les facteurs de risque les plus importants, on retrouve la
          résistance à l’insuline, qui est largement favorisée par
          l’alcool, la sédentarité, le tabac, l’obésité, le VIH/TARV et
          l’hérédité. Elle est aujourd’hui reconnue comme un facteur
          majeur du syndrome métabolique et de nombreuses maladies, notamment
          le diabète de type 2, les maladies cardiovasculaires, certains
          cancers et d’autres maladies cardiométaboliques. En réalisant un
          dépistage précoce de la résistance à l’insuline et en mettant
          en place des interventions concernant l’alimentation et
          l’activité physique, il est possible de prévenir de manière
          économique les maladies non transmissibles associées au syndrome
          métabolique`,text5:`Parmi les facteurs de risque les plus importants, on retrouve la
          résistance à l’insuline, qui est largement favorisée par
          l’alcool, la sédentarité, le tabac, l’obésité, le VIH/TARV et
          l’hérédité. Elle est aujourd’hui reconnue comme un facteur
          majeur du syndrome métabolique et de nombreuses maladies, notamment
          le diabète de type 2, les maladies cardiovasculaires, certains
          cancers et d’autres maladies cardiométaboliques. En réalisant un
          dépistage précoce de la résistance à l’insuline et en mettant
          en place des interventions concernant l’alimentation et
          l’activité physique, il est possible de prévenir de manière
          économique les maladies non transmissibles associées au syndrome
          métabolique`,text6:` En effet, il est important de noter que la résistance à
          l’insuline n’est pas une condamnation à vie. Si elle est
          détectée à temps, les personnes concernées possèdent une fenêtre de
          10 à 15 ans durant laquelle ils peuvent retrouver une sensibilité à
          l’insuline. Pour cela, des mesures appropriées doivent être mises en
          place, comprenant des changements de mode de vie tels qul’une
          alimentation équilibrée et une activité physique régulière.`,text7:`Chez Innov Biotech, notre mission est de lutter contre les maladies
          cardiométaboliques en proposant le test IDIR. Ce test offre une
          solution fiable, efficace et reproductible pour le dépistage de la
          résistance à l’insuline. En combinant la détection des
          symptômes cliniques de la résistance à l’insuline avec le test
          IDIR, nous permettons un diagnostic précis de cette condition au
          sein de la population, ainsi qu’une prévention des maladies
          cardiométaboliques. <br />
          Nous sommes déterminés à faire progresser la santé cardiovasculaire
          et métabolique, et nous espérons pouvoir contribuer à réduire
          l’impact de ces maladies à l’échelle mondiale.
          Rejoignez-nous dans notre lutte contre les maladies
          cardiométaboliques et découvrez comment notre test IDIR peut jouer
          un rôle essentiel dans la prévention et le dépistage précoce de ces
          affections.`,text8:`Chez Innov Biotech, notre mission est de lutter contre les maladies
          cardiométaboliques en proposant le test IDIR. Ce test offre une
          solution fiable, efficace et reproductible pour le dépistage de la
          résistance à l’insuline. En combinant la détection des
          symptômes cliniques de la résistance à l’insuline avec le test
          IDIR, nous permettons un diagnostic précis de cette condition au
          sein de la population, ainsi qu’une prévention des maladies
          cardiométaboliques. <br />
          Nous sommes déterminés à faire progresser la santé cardiovasculaire
          et métabolique, et nous espérons pouvoir contribuer à réduire
          l’impact de ces maladies à l’échelle mondiale.
          Rejoignez-nous dans notre lutte contre les maladies
          cardiométaboliques et découvrez comment notre test IDIR peut jouer
          un rôle essentiel dans la prévention et le dépistage précoce de ces
          affections.`}),!s&&e.jsxs(e.Fragment,{children:[e.jsx("div",{className:"header-img",children:e.jsx(o,{logo:d})}),e.jsx("section",{id:"notre-demarche",style:m,children:e.jsxs("div",{style:p,className:"container topbanniere",children:[e.jsx("div",{style:b,children:e.jsx("h1",{children:"NOTRE VISION"})}),e.jsx("div",{style:v,children:e.jsx("p",{children:"Notre démarche / Notre vision"})})]})}),e.jsx("h2",{className:"container",id:"titre2",children:"NOTRE OBJECTIF : ENRAYER L’ÉPIDÉMIE MONDIALE CAUSÉE PAR LES MALADIES CARDIOMÉTABOLIQUES"}),e.jsxs("p",{className:"container dispositif-medical",children:['Selon le rapport 2022 de l’OMS (Organisation mondiale de la santé) intitulé "Invisible numbers", les maladies non transmissibles, telles que les maladies cardiovasculaires et le diabète de type 2, représentent plus de 70% des décès dans le monde. Ces maladies ont des causes multiples, à la fois sociales, environnementales, commerciales et génétiques, et touchent toutes les régions du globe. Malheureusement, chaque année, 17 millions de personnes de moins de 70 ans meurent prématurément des suites de ces maladies.',e.jsx("br",{}),e.jsx("br",{}),"Cependant, bon nombre de ces décès prématurés pourraient être évités. Pour cela il est primordial de dépister les maladies cardiométaboliques (diabètes…) et de se concentrer sur la réduction des facteurs de risque associés à ces maladies, tels qu’une mauvaise alimentation et la sédentarité. Heureusement, des solutions peu coûteuses existent pour les gouvernements et les autres acteurs impliqués, afin de réduire ces facteurs de risque modifiables, tel que le programme PREDIA proposé par l’ONG AcSan.",e.jsx("br",{}),e.jsx("br",{}),"Parmi les facteurs de risque les plus importants, on retrouve la résistance à l’insuline, qui est largement favorisée par l’alcool, la sédentarité, le tabac, l’obésité, le VIH/TARV et l’hérédité. Elle est aujourd’hui reconnue comme un facteur majeur du syndrome métabolique et de nombreuses maladies, notamment le diabète de type 2, les maladies cardiovasculaires, certains cancers et d’autres maladies cardiométaboliques. En réalisant un dépistage précoce de la résistance à l’insuline et en mettant en place des interventions concernant l’alimentation et l’activité physique, il est possible de prévenir de manière économique les maladies non transmissibles associées au syndrome métabolique"]}),e.jsx("div",{className:"bgimg",children:e.jsx("img",{src:a,alt:"Dispositif",className:"img"})}),e.jsx("p",{className:"container dispositif-medical",children:"En effet, il est important de noter que la résistance à l’insuline n’est pas une condamnation à vie. Si elle est détectée à temps, les personnes concernées possèdent une fenêtre de 10 à 15 ans durant laquelle ils peuvent retrouver une sensibilité à l’insuline. Pour cela, des mesures appropriées doivent être mises en place, comprenant des changements de mode de vie tels qul’une alimentation équilibrée et une activité physique régulière."}),e.jsx("div",{className:"bgimg",children:e.jsx("img",{src:i,alt:"Dispositif",className:"img"})}),e.jsxs("p",{className:"container dispositif-medical",children:["Chez Innov Biotech, notre mission est de lutter contre les maladies cardiométaboliques en proposant le test IDIR. Ce test offre une solution fiable, efficace et reproductible pour le dépistage de la résistance à l’insuline. En combinant la détection des symptômes cliniques de la résistance à l’insuline avec le test IDIR, nous permettons un diagnostic précis de cette condition au sein de la population, ainsi qu’une prévention des maladies cardiométaboliques. ",e.jsx("br",{}),"Nous sommes déterminés à faire progresser la santé cardiovasculaire et métabolique, et nous espérons pouvoir contribuer à réduire l’impact de ces maladies à l’échelle mondiale. Rejoignez-nous dans notre lutte contre les maladies cardiométaboliques et découvrez comment notre test IDIR peut jouer un rôle essentiel dans la prévention et le dépistage précoce de ces affections."]}),e.jsx(u,{})]})]})},m={display:"flex",justifyContent:"center",alignItems:"center"},p={display:"flex",flexDirection:"row",alignItems:"center",width:"100%"},b={flex:1},v={display:"none",flex:1,textAlign:"right"};export{h as default};
